package exceptions;

import org.json.JSONObject;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletResponse;

/**
 * Exception generator
 */
public enum SXException {

    EXCEPTION_TODO(0, "Exception is not creted yet"), //TODO create exception logic

    INCORECT_LOGIN_OR_PASS(44, "Incorect login or password"),
    AUTHORIZATION_FAILED(45, "Authorization failed"),
    ACCOUNT_EXISTS(43, "Accaunt exists"),
    DBError (500, "DBError"),
    NOT_FOUND(404, "Resource not found");

    private int mCode;
    private String mMassage;

    SXException(int pCode, String pMassage) {
        mCode = pCode;
        mMassage = pMassage;
    }

    public int getCode() {
        return mCode;
    }

    public String getMassage() {
        return mMassage;
    }

    public static String print(HttpServletResponse pResp, SXException pType) {
        return print(pResp, pType, null);
    }

    public static String print(HttpServletResponse pResp, SXException pType, Exception e) {
        pResp.setStatus(200);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("Error", pType.getCode());
        jsonObject.put("Message", pType.getMassage());
        if (e != null) {
            jsonObject.put("Exception", e.getMessage());
        }
        return jsonObject.toString();
    }

}
