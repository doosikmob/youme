package entity;

import utils.TextUtils;

import javax.servlet.http.HttpServletRequest;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Utils for producers
 */
public class UProducer {

    public static int checkValidation(HttpServletRequest pRequest, Connection pConnection) throws SQLException {
        try {
            if (TextUtils.isEmpty(pRequest.getHeader("token"))) return -1;

            String token = pRequest.getHeader("token");
            final String CALL_CHECK_VALIDATION = "{call check_token(?)}";
            CallableStatement statementCheck = pConnection.prepareCall(CALL_CHECK_VALIDATION);
            statementCheck.setString(1, token);
            ResultSet resultSet = statementCheck.executeQuery();
            if (resultSet.next()) {
                int id = resultSet.getInt(resultSet.findColumn("id"));
                statementCheck.close();
                return id;

            } else {
                statementCheck.close();
                return -1;
            }
        } catch (Exception e) {
            return -1;
        }
    }

}
