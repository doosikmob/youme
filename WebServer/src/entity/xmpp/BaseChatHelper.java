package entity.xmpp;


import org.jivesoftware.smack.AccountManager;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smackx.muc.MultiUserChat;
import org.jivesoftware.smackx.xdata.Form;
import org.jivesoftware.smackx.xdata.FormField;
import utils.C;
import utils.U;

import java.io.IOException;
import java.util.Iterator;

/**
 * BaseChatHelper
 */
public class BaseChatHelper {



    private XMPPTCPConnection mChatConnection;


    public BaseChatHelper() throws XMPPException, IOException, SmackException {
        this(String.format("%s@%s", C.ADMIN_NAME, C.HOST), C.ADMIN_PASSWORD);
    }

    public BaseChatHelper(int pUserId, String pPassword) throws XMPPException, IOException, SmackException {
        this(String.format("u_%d@%s", pUserId, C.HOST), pPassword);
    }

    private BaseChatHelper(String pUserName, String pPassword) throws IOException, XMPPException, SmackException {
        ConnectionConfiguration connectionConfiguration = new ConnectionConfiguration(C.HOST, 5222);
        connectionConfiguration.setSecurityMode(ConnectionConfiguration.SecurityMode.disabled);
        connectionConfiguration.setCompressionEnabled(false);

        mChatConnection = new XMPPTCPConnection(connectionConfiguration);
        mChatConnection.connect();
        mChatConnection.login(pUserName, pPassword);

    }

    public AccountManager getAccountManager() {
        return AccountManager.getInstance(mChatConnection);
    }
    
    public void end(){
        if (mChatConnection != null && mChatConnection.isConnected()) {
            try {
                mChatConnection.disconnect();
            } catch (SmackException.NotConnectedException e) {
                e.printStackTrace();
            }
        }
    }

    public XMPPTCPConnection getChatConnection() {
        return mChatConnection;
    }

    public void setChatConnection(XMPPTCPConnection pChatConnection) {
        mChatConnection = pChatConnection;
    }


    public void registerAccount(String userName, String password) throws IOException, XMPPException, SmackException {
            getAccountManager().createAccount(userName, password);
    }

    public void deleteAccount() throws IOException, XMPPException, SmackException {
        getAccountManager().deleteAccount();
    }

    public void createRoom(String pUserName, String pRoomName, boolean pPublic, boolean pStatic) throws XMPPException, SmackException, IOException {
        MultiUserChat muc = new MultiUserChat(mChatConnection, pRoomName);
        muc.create(pRoomName);
        muc.grantOwnership(pUserName);


        Form form = muc.getConfigurationForm();
        Form submitForm = form.createAnswerForm();
        for (Iterator fields = form.getFields().iterator();fields.hasNext();){
            FormField field = (FormField) fields.next();
            if(!FormField.TYPE_HIDDEN.equals(field.getType()) && field.getVariable()!= null){
                submitForm.setDefaultAnswer(field.getVariable());
            }
        }
        if (pPublic)  {
            submitForm.setAnswer("muc#roomconfig_publicroom", true);
        }
        if (pStatic) {
            submitForm.setAnswer("muc#roomconfig_persistentroom", true);
        }
        muc.sendConfigurationForm(submitForm);
    }






}
