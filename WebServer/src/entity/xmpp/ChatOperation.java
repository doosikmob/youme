package entity.xmpp;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPException;
import utils.C;
import utils.U;

import java.io.IOException;

/**
 * ChatOperation
 */
public class ChatOperation {

    public static String getUserJID(int pUserId) {
        return String.format("u_%d@%s", pUserId, C.HOST);
    }


    public static String getChatPassword(String pLogin, String pPassword, int pId) {
        return U.MD5(String.format("%s_%d_%s_%s", pLogin, pId, C.SOLT, pPassword));
    }

    public static String getRoomJID(String pRoom) {
        return String.format("%s@conference.%s", pRoom, C.HOST);
    }

    public static void createUser(String pLogin, String pPassword, int pId) throws IOException, XMPPException, SmackException {
        BaseChatHelper chatBuilder = new BaseChatHelper();
        chatBuilder.registerAccount(String.format("u_%d", pId), pPassword);
        chatBuilder.end();
    }

    public static void deleteUser(int pUserId, String pPassword) throws IOException, XMPPException, SmackException {
        BaseChatHelper chatBuilder = new BaseChatHelper(pUserId, pPassword);
        chatBuilder.deleteAccount();
        chatBuilder.end();
    }

    public static String createRoom(String pRoom, int pUserId) {
        String roomJid = getRoomJID(pRoom);
        try {
            BaseChatHelper chatBuilder = new BaseChatHelper();
            chatBuilder.createRoom(getUserJID(pUserId), roomJid, true, true);
            chatBuilder.end();
        } catch (Exception e) {
            return null;
        }
        return roomJid;
    }
}
