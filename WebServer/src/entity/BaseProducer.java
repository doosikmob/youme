package entity;

import base.EMethod;
import exceptions.SXException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.net.URI;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import static exceptions.SXException.*;

/**
 * BaseProducer
 */
public abstract class BaseProducer {
    private final EMethod               mMethod;
    private HttpServletRequest          mRequest;
    private HttpServletResponse         mResponce;
    private Connection                  mConnection;
    private int                         mUserId;



    public BaseProducer(HttpServletRequest pRequest, HttpServletResponse pResponce, Connection pConnection, EMethod pMethod, int pUserId) {
        mRequest = pRequest;
        mResponce = pResponce;
        mConnection = pConnection;
        mMethod = pMethod;
        mUserId = pUserId;
    }

    public int getUserId() {
        return mUserId;
    }

    public HttpServletRequest getRequest() {
        return mRequest;
    }

    public HttpServletResponse getResponce() {
        return mResponce;
    }

    public Connection getConnection() {
        return mConnection;
    }



    public void produce() throws ServletException, IOException, SQLException, Exception {
        getResponce().getWriter().print("ok");
    }

    protected void print(String pPrint) throws IOException {
        getResponce().getWriter().print(pPrint);
    }

    public JSONArray getJSONArray (ResultSet pResultSet) {
        JSONArray jsonArray = new JSONArray();
        for (JSONObject obj : getFormattedResult(pResultSet)) {
            jsonArray.put(obj);
        }

        return jsonArray;
    }

    public JSONObject getJSONObject (ResultSet pResultSet) {
        List<JSONObject> list = getFormattedResult(pResultSet);
        return list.size() > 0 ? list.get(0) : new JSONObject();
    }

    private List<JSONObject> getFormattedResult(ResultSet rs) {
        List<JSONObject> resList = new ArrayList<JSONObject>();
        try {
            ResultSetMetaData rsMeta = rs.getMetaData();
            int columnCnt = rsMeta.getColumnCount();
            List<String> columnNames = new ArrayList<String>();
            for(int i=1;i<=columnCnt;i++) {
                columnNames.add(rsMeta.getColumnName(i));
            }

            while(rs.next()) { // convert each object to an human readable JSON object
                JSONObject obj = new JSONObject();
                for(int i=1;i<=columnCnt;i++) {
                    String key = columnNames.get(i - 1);
                    String value = rs.getString(i);
                    obj.put(key, value);
                }
                resList.add(obj);
            }
        } catch(Exception e) {
            e.printStackTrace();
        } finally {
            try {
                rs.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return resList;
    }

    protected JSONObject getJSONBody() throws Exception {
        StringBuffer jb = new StringBuffer();
        String line = null;
        try {
            BufferedReader reader = getRequest().getReader();
            while ((line = reader.readLine()) != null)
                jb.append(line);
        } catch (Exception e) { /*report an error*/ }

        try {
            JSONObject jsonObject = new JSONObject(jb.toString());
            return jsonObject;
        } catch (Exception e) {
            // crash and burn
            throw new IOException("Error parsing JSON request string");
        }

    }

    protected void printException(SXException pType) throws IOException {
        print(SXException.print( getResponce(), pType));
    }

    protected void printException(SXException pType, Exception e) throws IOException {
        print(SXException.print(getResponce(), pType, e));
    }

    protected String getBaseUrl () {
        URI uri = URI.create(mRequest.getRequestURL().toString());
        return uri.getHost();
    }

//    protected String getHeader(String pHeader) {
//        if (mRequest == null || mRequest.getHeaderNames() == null) return null;
//        Enumeration<String> en = mRequest.getHeaderNames();
//        while (en.hasMoreElements()) {
//            String header = en.nextElement();
//            if (pHeader.equals(header)) return mRequest.getHeader()
//        }
//    }

}
