package entity.rooms;

import base.EMethod;
import entity.BaseProducer;
import entity.xmpp.ChatOperation;
import org.json.JSONObject;
import utils.TextUtils;
import utils.U;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * AddRoomProducer
 */
public class AddRoomProducer extends BaseProducer {

    private static final String CALL_ADD_ROOM = "{call add_public_room(?, ?, ?, ?, ?, ?, ?)}";

    public AddRoomProducer(HttpServletRequest pRequest, HttpServletResponse pResponce, Connection pConnection, EMethod pMethod, Integer pUserId) {
        super(pRequest, pResponce, pConnection, pMethod, pUserId);
    }

    @Override
    public void produce() throws Exception {

        JSONObject obj = getJSONBody();

        String room_name = obj.getString("room_name");
        String hash = obj.getString("hash");
        long currentTime = System.currentTimeMillis();
        String newHash = U.MD5(String.format("%s_%d", hash, currentTime));

        double lat = obj.getDouble("lat");
        double lng = obj.getDouble("lng");
        int distance = obj.getInt("distance");

        String roomJid = ChatOperation.createRoom(newHash, getUserId());
        if (!TextUtils.isEmpty(roomJid)) {
            CallableStatement statementRegister = getConnection().prepareCall(CALL_ADD_ROOM);
            statementRegister.setString(1, roomJid);
            statementRegister.setString(2, room_name);
            statementRegister.setInt(3, getUserId());
            statementRegister.setDouble(4, lat);
            statementRegister.setDouble(5, lng);
            statementRegister.setLong(6, currentTime);
            statementRegister.setInt(7, distance);
            //print(U.getConcatNavicat(roomJid, room_name, getUserId(), lat, lng, currentTime, distance));
            statementRegister.executeQuery();

            JSONObject jsonObject  = new JSONObject();
            jsonObject.put("jid", roomJid);
            jsonObject.put("room_name", room_name);
            jsonObject.put("hash", hash);
            jsonObject.put("creation", currentTime);
            jsonObject.put("lat", lat);
            jsonObject.put("lng", lng);
            jsonObject.put("distance", distance);

            statementRegister.close();
            print(jsonObject.toString());
        }

    }
}
