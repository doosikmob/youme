package entity.rooms;

import base.EMethod;
import entity.BaseProducer;
import entity.xmpp.ChatOperation;
import exceptions.SXException;
import org.json.JSONArray;
import org.json.JSONObject;
import utils.U;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * SyncRoomsProducer
 */
public class SyncRoomsProducer extends BaseProducer {
    private static final String CALL_GET_ROOMS= "{call get_public_rooms(?, ?)}";

    public SyncRoomsProducer(HttpServletRequest pRequest, HttpServletResponse pResponce, Connection pConnection, EMethod pMethod, Integer pUserId) {
        super(pRequest, pResponce, pConnection, pMethod, pUserId);
    }

    @Override
    public void produce() throws ServletException, IOException, SQLException {
        double lat = Double.valueOf(getRequest().getHeader("lat"));
        double lng = Double.valueOf(getRequest().getHeader("lng"));

        CallableStatement statementRegister = getConnection().prepareCall(CALL_GET_ROOMS);
        statementRegister.setDouble(1, lat);
        statementRegister.setDouble(2, lng);

        ResultSet resultSet = statementRegister.executeQuery();
        JSONObject jsonBaseObj = new JSONObject();
        JSONArray jsonArray = new JSONArray();

        while (resultSet.next()) {

            String jid = resultSet.getString(resultSet.findColumn("jid"));
            String name = resultSet.getString(resultSet.findColumn("name"));
            String center_lat = resultSet.getString(resultSet.findColumn("center_lat"));
            String center_lng = resultSet.getString(resultSet.findColumn("center_lng"));
            String owner_id = resultSet.getString(resultSet.findColumn("owner_id"));
            String creation = resultSet.getString(resultSet.findColumn("creation"));
            String public_name = resultSet.getString(resultSet.findColumn("public_name"));

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("jid", jid);
            jsonObject.put("name", name);
            jsonObject.put("center_lat", center_lat);
            jsonObject.put("center_lng", center_lng);
            jsonObject.put("owner_id", owner_id);
            jsonObject.put("creation", creation);
            jsonObject.put("public_name", public_name);
            jsonArray.put(jsonObject);
        }

        jsonBaseObj.put("public_rooms", jsonArray);
        print(jsonBaseObj.toString());
        statementRegister.close();
    }


}
