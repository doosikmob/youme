package entity.auth;

import base.EMethod;
import entity.BaseProducer;
import entity.xmpp.ChatOperation;
import exceptions.SXException;
import org.json.JSONObject;
import utils.U;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * ValidationProducer
 */
public class ValidationProducer extends BaseProducer {


    private static final String CALL_CHECK_LOGIN_PASS = "{call check_login_pass(?, ?, ?)}";

    public ValidationProducer(HttpServletRequest pRequest, HttpServletResponse pResponce, Connection pConnection, EMethod pMethod, Integer pUserId) {
        super(pRequest, pResponce, pConnection, pMethod, pUserId);
    }

    @Override
    public void produce() throws ServletException, IOException, SQLException {
        String login = getRequest().getHeader("login");
        if (login != null) {
            login = login.toLowerCase();
        }

        String password = getRequest().getHeader("password");
        String date = U.getExpireDate();
        String token = U.createToken(login, password, getBaseUrl(), date);
        CallableStatement statementRegister = getConnection().prepareCall(CALL_CHECK_LOGIN_PASS);
        statementRegister.setString(1, login);
        statementRegister.setString(2, U.MD5(password));
        statementRegister.setString(3, token);

            try {
                ResultSet resultSet = statementRegister.executeQuery();
                if (resultSet.next()) {

                    int userId = resultSet.getInt(resultSet.findColumn("id"));
                    String tokenResult = resultSet.getString(resultSet.findColumn("token"));
                    String expireDate = resultSet.getString(resultSet.findColumn("reg_date"));
                    String publicName = resultSet.getString(resultSet.findColumn("public_name"));
                    String chatPass = ChatOperation.getChatPassword(login, password, userId);

                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("UserName", login);
                    jsonObject.put("Token", tokenResult);
                    jsonObject.put("ExpireDate", expireDate);
                    jsonObject.put("PublicName", publicName);
                    jsonObject.put("UserId", String.valueOf(userId));
                    jsonObject.put("JID", ChatOperation.getUserJID(userId));
                    jsonObject.put("ChatPassword", chatPass);
                    print(jsonObject.toString());

                }
                else {
                    printException(SXException.INCORECT_LOGIN_OR_PASS);
                }




            } catch (Exception e) {
                printException(SXException.DBError, e);
            }
            statementRegister.close();
    }


}
