package entity.auth;

import base.EMethod;
import entity.BaseProducer;
import entity.xmpp.ChatOperation;
import exceptions.SXException;
import org.json.JSONObject;
import utils.U;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;

/**
 * SyncRoomsProducer
 */
public class RegistrationProducer extends BaseProducer {

    private static final String CALL_CHECK_LOGIN = "{call check_login(?)}";
    private static final String CALL_REGISTER = "{call register_new(?,?,?,?,?,?)}";

    public RegistrationProducer(HttpServletRequest pRequest, HttpServletResponse pResponce, Connection pConnection, EMethod pMethod, Integer pUserId) {
        super(pRequest, pResponce, pConnection, pMethod, pUserId);
    }

    @Override
    public void produce() throws ServletException, IOException, SQLException {
        String login = getRequest().getHeader("login");
        if (login != null) {
            login = login.toLowerCase();
        }
        String password = getRequest().getHeader("password");
        String publicName = getRequest().getHeader("public_name");

        SXException error = isCheck(login, password, publicName);
        if (error == null) {
            String date = U.getExpireDate();
            String token = U.createToken(login, password, getBaseUrl(), date);
            try  {
                CallableStatement statementRegister = getConnection().prepareCall(CALL_REGISTER);
                statementRegister.setString(1, login);
                statementRegister.setString(2, U.MD5(password));
                statementRegister.setString(3, publicName);
                statementRegister.setString(4, token);
                statementRegister.setString(5, date);
                statementRegister.registerOutParameter(6, Types.INTEGER);
                statementRegister.executeQuery();
                int id = statementRegister.getInt(6);
                String chatPass = ChatOperation.getChatPassword(login, password, id);

                ChatOperation.createUser(login, chatPass, id);

                JSONObject jsonObject = new JSONObject();
                jsonObject.put("UserName", login);
                jsonObject.put("PublicName", publicName);
                jsonObject.put("Token", token);
                jsonObject.put("UserId", id);
                jsonObject.put("ExpireDate", date);
                jsonObject.put("JID", ChatOperation.getUserJID(id));
                jsonObject.put("ChatPassword", chatPass);

                print(jsonObject.toString());

            } catch (Exception e) {
                printException(SXException.EXCEPTION_TODO, e);
            }

        } else {
            printException(error);
        }

    }


    private SXException isCheck(String pLogin, String pPassword, String pPublicName) throws SQLException {
        if (pLogin == null || pPassword == null || !U.checkEmail(pLogin) || pPublicName == null || pPublicName.length() < 4) return SXException.INCORECT_LOGIN_OR_PASS;

        CallableStatement statementCheck = getConnection().prepareCall(CALL_CHECK_LOGIN);
        statementCheck.setString(1, pLogin);
        boolean check = !statementCheck.executeQuery().next();
        statementCheck.close();
        return check ? null : SXException.ACCOUNT_EXISTS;
    }
}
