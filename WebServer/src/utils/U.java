package utils;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Utils
 */
public class U {

    public static String getExpireDate() {
        Date date = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.DATE, C.EXPIRE_DAYS);
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sf.format(c.getTime());
    }

    public static String createToken(String pLogin, String pPassword, String pUrl, String pDate) {
        return MD5(String.format("%s_%s_%s_%s_%s", pLogin, pPassword, pUrl, pDate, C.SOLT));
    }

    public static String MD5(String st) {
        MessageDigest messageDigest;
        byte[] digest = new byte[0];

        try {
            messageDigest = MessageDigest.getInstance("MD5");
            messageDigest.reset();
            messageDigest.update(st.getBytes());
            digest = messageDigest.digest();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        BigInteger bigInt = new BigInteger(1, digest);
        String md5Hex = bigInt.toString(16);

        while (md5Hex.length() < 32) {
            md5Hex = "0" + md5Hex;
        }

        return md5Hex;
    }


    public static boolean checkEmail(String email) {
        Pattern p = Pattern.compile(".+@.+\\.[a-z]+");
        Matcher m = p.matcher(email);
        return m.matches();
    }

    public static String getConcatNavicat(Object... pObjects) {
        String str = "";
        for (int i = 0; i < pObjects.length; i++) {
            str = str + "'" + String.valueOf(pObjects[i]) + "'";
            if (i != pObjects.length - 1) {
                str  = str + ", ";
            }
        }
        return str;

    }


}
