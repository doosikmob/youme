package base;

import entity.auth.RegistrationProducer;
import entity.auth.ValidationProducer;
import entity.rooms.AddRoomProducer;
import entity.rooms.SyncRoomsProducer;

/**
 * Path information
 */
public enum EPath {
    UNKNOWN("", false, null, EMethod.GET),
    REGISTER("register", false, RegistrationProducer.class, EMethod.POST),
    VALIDATE("validate", false, ValidationProducer.class, EMethod.POST),
    SYNC_ROOMS("sync_rooms", true, SyncRoomsProducer.class, EMethod.POST),
    ADD_ROOM("add_room", true, AddRoomProducer.class, EMethod.POST);


    private boolean     mAuthorization;
    private String      mPath;
    Class<?>            mClass;
    EMethod             mMethod;

    EPath(String pPath, boolean pAuthorization, Class<?> tClass, EMethod pMethod) {
        this.mAuthorization = pAuthorization;
        this.mPath = pPath;
        mClass = tClass;
        mMethod = pMethod;

    }

    public boolean isAuthorization() {
        return mAuthorization;
    }

    public String getPath() {
        return mPath;
    }

    public Class<?> getProducerClass() {
        return mClass;
    }

    public static EPath getPath(String basePath, EMethod pMethod) {
        String path = getFormattedPath(basePath);
        for (EPath value : values()) {
            if (value.getPath().toLowerCase().matches(String.format("^%s$",path)) && pMethod == value.getMethod()) {
                return value;
            }
        }

        return UNKNOWN;
    }

    public EMethod getMethod() {
        return mMethod;
    }

    private static String getFormattedPath(String basePath) {
        String path = basePath.toLowerCase();
        if (path.startsWith("/")) {
            path = path.substring(1);
        }
        if (path.endsWith("/")) {
            path = path.substring(0, path.length() - 1);
        }
        return path;
    }
}
