import entity.BaseProducer;
import base.EMethod;
import base.EPath;
import entity.UProducer;
import exceptions.SXException;

import javax.naming.InitialContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.*;
import java.sql.*;
import java.util.Enumeration;

/**
 * BaseServlet
 */
public class BaseServlet <T extends BaseProducer> extends HttpServlet {

    private static final String DATA_RESOURCE = "SXTeamRes";

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doOperation(req, resp, EMethod.POST);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doOperation(req, resp, EMethod.GET);
    }

    private void doOperation(HttpServletRequest req, HttpServletResponse resp , EMethod pMethod) throws IOException {
        String basePath = req .getPathInfo();
        boolean isDebug = isDebug(basePath);
        EPath path = EPath.getPath(basePath, pMethod);

        InitialContext ctx;
        Connection connection = null;
        try {
            ctx = new InitialContext();
            DataSource ds = (DataSource) ctx.lookup(DATA_RESOURCE);
            connection = ds.getConnection();
            if (path != EPath.UNKNOWN ) {
                int id = path.isAuthorization() ? UProducer.checkValidation(req, connection) : -1;
                if (id > 0 || !path.isAuthorization()) {
                    Class TS = Class.forName(path.getProducerClass().getName());
                    T producer = (T) TS.getConstructor(HttpServletRequest.class, HttpServletResponse.class, Connection.class, EMethod.class, Integer.class)
                            .newInstance(req, resp, connection, pMethod, id);
                    producer.produce();
                } else {
                    resp.getWriter().print(SXException.print(resp, SXException.AUTHORIZATION_FAILED));

                }
            } else {
                resp.getWriter().print(SXException.print(resp, SXException.NOT_FOUND));
            }

        } catch (SQLException e) {
            if (isDebug) {
                printDebug(req, resp, e);
            } else {
                printToFile(e);
            }
        } catch (Exception e) {
            if (isDebug) {
                printDebug(req, resp, e);
            } else {
                printToDB(e);
            }
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    //TODO Connection close exception
                }
            }
        }
    }

    private void printDebug(HttpServletRequest req, HttpServletResponse resp, Exception e) throws IOException {
        resp.getWriter().println(String.format("url : %s", req.getRequestURL().toString()));
        Enumeration<String> en = req.getHeaderNames();
        while (en.hasMoreElements()) {
            String header = en.nextElement();
            resp.getWriter().println(String.format("%s : %s", header, req.getHeader(header)));
        }
        resp.getWriter().print(getStackTrace(e));
    }

    private String getStackTrace(Exception e) {
        Writer writer = new StringWriter();
        PrintWriter printWriter = new PrintWriter(writer);
        e.printStackTrace(printWriter);
        return writer.toString();
    }

    private void printToDB(Exception e) {
        //TODO
    }

    private void printToFile(SQLException e) {
        //TODO
    }

    private boolean isDebug(String path) {
        //TODO
        return true;
    }
}
