/*
Navicat MySQL Data Transfer

Source Server         : 178.20.157.58
Source Server Version : 50544
Source Host           : 178.20.157.58:3306
Source Database       : SXTeam

Target Server Type    : MYSQL
Target Server Version : 50544
File Encoding         : 65001

Date: 2015-12-15 20:36:14
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for Auth
-- ----------------------------
DROP TABLE IF EXISTS `Auth`;
CREATE TABLE `Auth` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `login` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `token` varchar(80) NOT NULL,
  `reg_date` datetime NOT NULL,
  `name` varchar(511) NOT NULL,
  `creation_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `vip` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `role` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `update_token` tinyint(3) unsigned NOT NULL,
  `public_name` varchar(255) DEFAULT NULL,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `geo_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`,`token`),
  UNIQUE KEY `id_index` (`id`) USING BTREE,
  UNIQUE KEY `token_index` (`token`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of Auth
-- ----------------------------
INSERT INTO `Auth` VALUES ('1', 'sdds', 'dsds', 'dssdsds', '0000-00-00 00:00:00', 'dsdsds', '2015-12-12 17:58:30', '0', '0', '0', null, null, null, null);
INSERT INTO `Auth` VALUES ('2', 'hjjjss2d2j@ddj.rr', '1efe1d87e78f6036c5914be35817a5c7', '1a9a88f71e691bbbea1119cf992e5da5', '2016-01-11 20:32:31', 'cxzczxc', '2015-12-12 18:32:34', '0', '0', '0', null, null, null, null);
INSERT INTO `Auth` VALUES ('3', 'hjjjss2d2j@d876dj.rr', '1efe1d87e78f6036c5914be35817a5c7', 'cde9c3a359d41c5492b5eb083e619c24', '2016-01-11 20:38:05', 'cxzczxc', '2015-12-12 18:38:09', '0', '0', '0', null, null, null, null);
INSERT INTO `Auth` VALUES ('4', 'hjjjss2td2j@d876dj.rr', '1efe1d87e78f6036c5914be35817a5c7', 'e02a7f087948bf43f6788e63f8fab8ad', '2016-01-11 20:41:00', 'cxzczxc', '2015-12-12 18:41:03', '0', '0', '0', null, null, null, null);
INSERT INTO `Auth` VALUES ('5', 'hdsjjjssd2d2j@ddj.rr', '0bf0a97d940068c46c0f7c7c0c58274a', '5d7e9874b0eeecb0a4844a6460853c98', '2016-01-11 20:42:06', 'cxzczxcd', '2015-12-12 18:42:06', '0', '0', '0', null, null, null, null);
INSERT INTO `Auth` VALUES ('6', '2d2j@ddj.rr', '0bf0a97d940068c46c0f7c7c0c58274a', 'da418068387d864fe5aa394efc0649e6', '2016-01-11 20:43:16', 'cxzczxcd', '2015-12-12 18:43:16', '0', '0', '0', null, null, null, null);
INSERT INTO `Auth` VALUES ('7', 'doosikd@ff.ff', '72c192c3084a2cacb1614e87f579fdd8', '2d8abb02d07ee5c4f306e2fbfb977dea', '2016-01-11 21:21:17', 'ggdhhyyy', '2015-12-12 19:21:17', '0', '0', '0', null, null, null, null);
INSERT INTO `Auth` VALUES ('8', 'doofffsikd@ff.ff', '72c192c3084a2cacb1614e87f579fdd8', 'd41f1b6c9d003ea227cb3c5a55556d07', '2016-01-11 21:21:37', 'ggdhhyyy', '2015-12-12 19:21:37', '0', '0', '0', null, null, null, null);
INSERT INTO `Auth` VALUES ('9', 'doosikggg@dc.fgd', 'f0b7b96505a29962f181d2e181e3e6d1', '92b9f9e3a05f6c9d329512f4cdab52f4', '2016-01-11 21:24:46', 'doosik2601455', '2015-12-12 19:24:47', '0', '0', '0', null, null, null, null);
INSERT INTO `Auth` VALUES ('10', 'doosikggg@dc.fg', 'f0b7b96505a29962f181d2e181e3e6d1', '58f4ee394cae2b9407e63ddf3ab791da', '2016-01-11 21:24:46', 'doosik2601455', '2015-12-12 19:24:47', '0', '0', '0', null, null, null, null);
INSERT INTO `Auth` VALUES ('12', 'doosikgdgg@dc.fgd', 'f0b7b96505a29962f181d2e181e3e6d1', '4a098023a44866c4081d668f91bccbbb', '2016-01-11 21:24:46', 'doosik2601455', '2015-12-12 19:24:47', '0', '0', '0', null, null, null, null);
INSERT INTO `Auth` VALUES ('13', 'dfrg@rtt.yy', '8d8c25f51e79801ee5ee5ed02b341b07', '9635bf57b35bf773db799216e5a504c0', '2016-01-11 21:26:19', 'erhdryrt', '2015-12-12 19:26:19', '0', '0', '0', null, null, null, null);
INSERT INTO `Auth` VALUES ('14', 'dfrg@rtt.yye', '8d8c25f51e79801ee5ee5ed02b341b07', '8b90f8c604987626a891e9d201014ea0', '2016-01-11 21:26:47', 'erhdryrt', '2015-12-12 19:26:47', '0', '0', '0', null, null, null, null);
INSERT INTO `Auth` VALUES ('15', 'ffd@swtt.tf', '6735f3c31563433d4e3ae8fdb732d06e', 'a7fb3906b1b757b0a6909f9f88eae8d6', '2016-01-11 21:42:02', 'fsfgg', '2015-12-12 19:42:02', '0', '0', '0', null, null, null, null);
INSERT INTO `Auth` VALUES ('16', 'Doosik@dd.dd', 'd8578edf8458ce06fbc5bb76a58c5ca4', '23a3f08930bb3178b3d2fd4beb7d153e', '2016-01-11 22:06:37', 'qwerty', '2015-12-12 20:06:37', '0', '0', '0', null, null, null, null);
INSERT INTO `Auth` VALUES ('17', 'hhh@gg.hh', 'b3d2450f770bbc022a96863e08cca481', 'c5523d36576826322d02c90a1f8b8c20', '2016-01-11 22:09:09', 'hhfcjug', '2015-12-12 20:09:09', '0', '0', '0', null, null, null, null);
INSERT INTO `Auth` VALUES ('18', 'jjhh@vgg.hh', '3ffcd1064661c86b19b79997e58c8e1e', '21c2d89f275d00f79fd21bc10e790032', '2016-01-11 22:12:12', 'bhhbbh', '2015-12-12 20:12:12', '0', '0', '0', null, null, null, null);
INSERT INTO `Auth` VALUES ('19', 'fndhr@ejeh.dd', '5a421660f86cfc92e66ee9ebfbd9f339', '68f8921225918abe7f62846dd5735357', '2016-01-11 22:24:57', 'fjrrjrj', '2015-12-12 20:24:57', '0', '0', '0', null, null, null, null);
INSERT INTO `Auth` VALUES ('20', 'dddr@dhdh.hh', '33e65218240b9484b0cfb375a0eddd6d', 'dd3b3e190225fb7ba8039fc06ec78add', '2016-01-11 22:26:32', 'hrjjjbfb', '2015-12-12 20:26:32', '0', '0', '0', null, null, null, null);
INSERT INTO `Auth` VALUES ('21', 'hhbh@gh.hh', '72ec48af90f1c27505321d3f4a7b9942', '951675770c79866fca6827eb5d60f086', '2016-01-11 22:29:40', 'hhtjj', '2015-12-12 20:29:40', '0', '0', '0', null, null, null, null);
INSERT INTO `Auth` VALUES ('22', 'djjd@dhhr.dj', 'fa3396eca05aa5631fdba3790c5e38ba', 'c51fb326a1ca05c251b20f542fed468a', '2016-01-11 22:31:18', 'djhrhdjh', '2015-12-12 20:31:18', '0', '0', '0', null, null, null, null);
INSERT INTO `Auth` VALUES ('23', 'djdj@dhhd.dd', 'd155f7ade8b11f0f381c5f555ca5f2dd', 'b29c79827ae72f3916773fb9d9f5fbc3', '2016-01-11 22:41:13', 'jjuxyg', '2015-12-12 20:41:13', '0', '0', '0', null, null, null, null);
INSERT INTO `Auth` VALUES ('24', 'ffgvff@tfg.gh', '2dbf1332558957266fb968f39509c211', 'b33113074e7ce60e2d6e20a4df27381a', '2016-01-11 23:39:48', 'tfhcdgbfdd', '2015-12-12 21:39:48', '0', '0', '0', null, null, null, null);
INSERT INTO `Auth` VALUES ('25', 'hhhh@ffv.hj', 'b47732ae48998b9cf19c36c12c30548d', 'de41e38ca048023d34bc5bc570ee0943', '2016-01-11 23:42:43', 'hgdjjvgg', '2015-12-12 21:42:43', '0', '0', '0', null, null, null, null);
INSERT INTO `Auth` VALUES ('26', 'rfgvc@thh.hj', '95c534c0b8c4e4cfbc76a7dfd6ab7324', 'f9a1d36cadd70be01e9315f9aa0e966a', '2016-01-11 23:43:37', 'ghjfthv', '2015-12-12 21:43:37', '0', '0', '0', null, null, null, null);
INSERT INTO `Auth` VALUES ('27', 'doosikjob@gmail.com', '5171d3a787c5e47bc6d25fe5660f47ab', 'ca451c19ea4dd2d030854d1e1cdf5436', '2016-01-13 21:59:34', 'doosik2601', '2015-12-12 22:07:22', '0', '0', '0', null, null, null, null);
INSERT INTO `Auth` VALUES ('28', 'doosikadw@gmail.com', 'd8578edf8458ce06fbc5bb76a58c5ca4', '44bf1c42012fc14a52226dcee3334e8b', '2016-01-12 14:26:28', 'qwerty', '2015-12-13 12:26:28', '0', '0', '0', null, null, null, null);
INSERT INTO `Auth` VALUES ('29', 'doosikadws@gmail.com', '49515482209a66a89d55568f70db5a52', '6e55d87ac429e116c50a5856b06890bf', '2016-01-12 14:26:43', 'qwertyrr', '2015-12-13 12:26:43', '0', '0', '0', null, null, null, null);
INSERT INTO `Auth` VALUES ('30', 'litera1994@gmail.com', '5109ae050e66b049130feeacfcafb10d', 'd260a2e362d45f2f08ab0d8b5184e44d', '2016-01-13 21:36:06', 'qazwsxedc123', '2015-12-13 19:34:53', '0', '0', '0', null, null, null, null);
INSERT INTO `Auth` VALUES ('31', 'doosik@mmail.ru', '5171d3a787c5e47bc6d25fe5660f47ab', 'e4cfa96e9a54f66d5e88aa339de3a836', '2016-01-13 21:36:01', 'doosik2601', '2015-12-14 19:36:02', '0', '0', '0', null, null, null, null);
INSERT INTO `Auth` VALUES ('32', 'dddd@dddd.dd', 'd8578edf8458ce06fbc5bb76a58c5ca4', '893c04d0c93e75f84ec68c8992900d75', '2016-01-13 21:50:17', 'qwerty', '2015-12-14 19:50:17', '0', '0', '0', null, null, null, null);
INSERT INTO `Auth` VALUES ('33', '2d2j@ddj.rrk', '160ed670b215888fe45280f30c096c8e', '47c93842f5440e0be450964bc9d075be', '2016-01-13 21:51:17', 'cxzczxcdk', '2015-12-14 19:51:17', '0', '0', '0', null, null, null, null);
INSERT INTO `Auth` VALUES ('34', 'dddd@dddad.dd', '6eea9b7ef19179a06954edd0f6c05ceb', '000d773c9f05799cea8deb24966b4f22', '2016-01-13 21:51:29', 'qwertyuiop', '2015-12-14 19:51:29', '0', '0', '0', null, null, null, null);
INSERT INTO `Auth` VALUES ('35', 'dddds@dddds.ds', 'd8578edf8458ce06fbc5bb76a58c5ca4', 'ee1c783b23038f0f9c10037c7a685d19', '2016-01-13 22:01:15', 'qwerty', '2015-12-14 20:01:15', '0', '0', '0', null, null, null, null);
INSERT INTO `Auth` VALUES ('36', 'hjdj@xhhh.ru', '654039da8498f367bf11594e2fbdb2f9', 'f95688ab8d3a35c3a6e5288200f0ab12', '2016-01-13 22:05:13', 'xxbxbbd', '2015-12-14 20:05:13', '0', '0', '0', null, null, null, null);
INSERT INTO `Auth` VALUES ('37', 'hbhx@djje.rj', '5a478c96836edbafafcb7abd99901036', 'd9d38034c7c42eea36bc1a119037c38d', '2016-01-13 22:06:15', 'xjjejej', '2015-12-14 20:06:15', '0', '0', '0', null, null, null, null);
INSERT INTO `Auth` VALUES ('38', 'xjddjj@dhdh.hhh', '4daea7f56b53b7034739b7de3d9ff166', '2d48a2769988b3dfec09b13161508634', '2016-01-13 22:14:16', 'xuhdhhsh', '2015-12-14 20:14:16', '0', '0', '0', null, null, null, null);
INSERT INTO `Auth` VALUES ('39', 'djhdhd@dhjd.dj', '3c1c1cbc090cb21bb03c6bc699df8a89', 'b18f574d73b625ed156044d353928a5e', '2016-01-13 22:16:33', 'dhddhdbdj', '2015-12-14 20:16:33', '0', '0', '0', null, null, null, null);
INSERT INTO `Auth` VALUES ('40', 'fjhhjdjjdd@djdn.dd', '10764659bd02d408a11e36205b645e99', '3f8ddaadd2800517cf592aa15b96c533', '2016-01-13 22:22:10', 'rjhfjdudhhd', '2015-12-14 20:22:10', '0', '0', '0', null, null, null, null);
INSERT INTO `Auth` VALUES ('41', 'ffff@ffff.ff', 'a98f6f64e6cdfac22ab2ffd15a7241e3', 'cbfd605f0d8ac50b94c9858ef186a288', '2016-01-13 22:23:41', 'fffff', '2015-12-14 20:23:41', '0', '0', '0', null, null, null, null);
INSERT INTO `Auth` VALUES ('42', 'djdjjdj@rjjd.ruxn', '8f667f11151a61c8385cd4266607b39d', '309b3a408fac1fe8bc94600611138c85', '2016-01-13 22:26:00', 'dnjdjenen', '2015-12-14 20:26:00', '0', '0', '0', null, null, null, null);
INSERT INTO `Auth` VALUES ('43', 'djhdhxhh@xhhd.ru', 'd77a5ee4228a1b8b95cd4f0666fed182', 'babfd4dda2d72377aec28d27672f5e00', '2016-01-13 22:41:11', 'dhhjhdhhe', '2015-12-14 20:41:11', '0', '0', '0', null, null, null, null);
INSERT INTO `Auth` VALUES ('44', 'djdhbr@djd.ru', '9c0c80d6d79c124eb15f2eb5275c3995', 'df3572d990dce81c4c11fb181d4eb4e5', '2016-01-13 22:43:35', 'fjfbdhchr', '2015-12-14 20:43:35', '0', '0', '0', null, null, null, null);
INSERT INTO `Auth` VALUES ('45', 'xdjrjrh@rjdhhh.fjfj', '87c349f4a571f997969a3c0df0ed35a2', '11f5c05f56aa96843d6a54b0af8a38a6', '2016-01-13 22:44:44', 'jddieiehrhh', '2015-12-14 20:44:44', '0', '0', '0', null, null, null, null);
INSERT INTO `Auth` VALUES ('46', 'djxjdd@ejdj.rjd', '782cec240665da680ed3f3b3df312911', '7027891f21a4667c53bfaa56668d0a07', '2016-01-13 23:09:41', 'xjjdjdjdd', '2015-12-14 21:09:41', '0', '0', '0', null, null, null, null);
INSERT INTO `Auth` VALUES ('47', 'fffddf@tfd.ff', 'd33b009db98139087415b62a666bf950', 'ddssdsd2121', '2016-01-14 18:23:52', 'tdrhhhd', '2015-12-14 21:13:25', '0', '0', '0', null, null, null, null);

-- ----------------------------
-- Table structure for UserInfo
-- ----------------------------
DROP TABLE IF EXISTS `UserInfo`;
CREATE TABLE `UserInfo` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(511) NOT NULL,
  `phone` decimal(12,0) DEFAULT NULL,
  `age` tinyint(4) NOT NULL DEFAULT '0',
  `sex` tinyint(4) NOT NULL DEFAULT '0',
  `orientation` tinyint(4) NOT NULL,
  `photo` varchar(511) NOT NULL,
  `raiting` int(11) NOT NULL DEFAULT '0',
  `money` int(11) DEFAULT NULL,
  `description` varchar(511) DEFAULT NULL,
  `reff_coupon` varchar(7) DEFAULT '0',
  `garem_cost` int(11) NOT NULL DEFAULT '0',
  `id_reg` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`,`id_reg`),
  UNIQUE KEY `id_index` (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=253 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of UserInfo
-- ----------------------------
INSERT INTO `UserInfo` VALUES ('1', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '1');
INSERT INTO `UserInfo` VALUES ('2', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '2');
INSERT INTO `UserInfo` VALUES ('3', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '3');
INSERT INTO `UserInfo` VALUES ('4', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '4');
INSERT INTO `UserInfo` VALUES ('5', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '5');
INSERT INTO `UserInfo` VALUES ('6', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '6');
INSERT INTO `UserInfo` VALUES ('7', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '7');
INSERT INTO `UserInfo` VALUES ('8', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '8');
INSERT INTO `UserInfo` VALUES ('9', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '9');
INSERT INTO `UserInfo` VALUES ('10', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '10');
INSERT INTO `UserInfo` VALUES ('11', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '11');
INSERT INTO `UserInfo` VALUES ('12', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '12');
INSERT INTO `UserInfo` VALUES ('13', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '13');
INSERT INTO `UserInfo` VALUES ('14', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '14');
INSERT INTO `UserInfo` VALUES ('15', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '15');
INSERT INTO `UserInfo` VALUES ('16', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '16');
INSERT INTO `UserInfo` VALUES ('17', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '17');
INSERT INTO `UserInfo` VALUES ('18', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '18');
INSERT INTO `UserInfo` VALUES ('19', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '19');
INSERT INTO `UserInfo` VALUES ('20', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '20');
INSERT INTO `UserInfo` VALUES ('21', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '21');
INSERT INTO `UserInfo` VALUES ('22', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '22');
INSERT INTO `UserInfo` VALUES ('23', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '23');
INSERT INTO `UserInfo` VALUES ('24', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '24');
INSERT INTO `UserInfo` VALUES ('25', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '25');
INSERT INTO `UserInfo` VALUES ('26', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '26');
INSERT INTO `UserInfo` VALUES ('27', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '27');
INSERT INTO `UserInfo` VALUES ('28', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '28');
INSERT INTO `UserInfo` VALUES ('29', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '29');
INSERT INTO `UserInfo` VALUES ('30', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '30');
INSERT INTO `UserInfo` VALUES ('31', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '31');
INSERT INTO `UserInfo` VALUES ('32', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '32');
INSERT INTO `UserInfo` VALUES ('33', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '33');
INSERT INTO `UserInfo` VALUES ('34', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '34');
INSERT INTO `UserInfo` VALUES ('35', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '35');
INSERT INTO `UserInfo` VALUES ('36', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '36');
INSERT INTO `UserInfo` VALUES ('37', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '37');
INSERT INTO `UserInfo` VALUES ('38', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '38');
INSERT INTO `UserInfo` VALUES ('39', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '39');
INSERT INTO `UserInfo` VALUES ('40', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '40');
INSERT INTO `UserInfo` VALUES ('41', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '41');
INSERT INTO `UserInfo` VALUES ('42', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '42');
INSERT INTO `UserInfo` VALUES ('43', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '43');
INSERT INTO `UserInfo` VALUES ('44', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '44');
INSERT INTO `UserInfo` VALUES ('45', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '45');
INSERT INTO `UserInfo` VALUES ('46', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '46');
INSERT INTO `UserInfo` VALUES ('47', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '47');
INSERT INTO `UserInfo` VALUES ('48', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '48');
INSERT INTO `UserInfo` VALUES ('49', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '49');
INSERT INTO `UserInfo` VALUES ('50', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '50');
INSERT INTO `UserInfo` VALUES ('51', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '51');
INSERT INTO `UserInfo` VALUES ('52', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '52');
INSERT INTO `UserInfo` VALUES ('53', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '53');
INSERT INTO `UserInfo` VALUES ('54', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '54');
INSERT INTO `UserInfo` VALUES ('55', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '55');
INSERT INTO `UserInfo` VALUES ('56', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '56');
INSERT INTO `UserInfo` VALUES ('57', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '57');
INSERT INTO `UserInfo` VALUES ('58', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '58');
INSERT INTO `UserInfo` VALUES ('59', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '59');
INSERT INTO `UserInfo` VALUES ('60', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '60');
INSERT INTO `UserInfo` VALUES ('61', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '61');
INSERT INTO `UserInfo` VALUES ('62', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '62');
INSERT INTO `UserInfo` VALUES ('63', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '63');
INSERT INTO `UserInfo` VALUES ('64', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '64');
INSERT INTO `UserInfo` VALUES ('65', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '65');
INSERT INTO `UserInfo` VALUES ('66', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '66');
INSERT INTO `UserInfo` VALUES ('67', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '67');
INSERT INTO `UserInfo` VALUES ('68', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '68');
INSERT INTO `UserInfo` VALUES ('69', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '69');
INSERT INTO `UserInfo` VALUES ('70', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '70');
INSERT INTO `UserInfo` VALUES ('71', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '71');
INSERT INTO `UserInfo` VALUES ('72', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '72');
INSERT INTO `UserInfo` VALUES ('73', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '73');
INSERT INTO `UserInfo` VALUES ('74', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '75');
INSERT INTO `UserInfo` VALUES ('75', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '77');
INSERT INTO `UserInfo` VALUES ('76', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '78');
INSERT INTO `UserInfo` VALUES ('77', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '79');
INSERT INTO `UserInfo` VALUES ('78', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '80');
INSERT INTO `UserInfo` VALUES ('79', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '81');
INSERT INTO `UserInfo` VALUES ('80', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '82');
INSERT INTO `UserInfo` VALUES ('81', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '83');
INSERT INTO `UserInfo` VALUES ('82', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '84');
INSERT INTO `UserInfo` VALUES ('83', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '85');
INSERT INTO `UserInfo` VALUES ('84', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '86');
INSERT INTO `UserInfo` VALUES ('85', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '87');
INSERT INTO `UserInfo` VALUES ('86', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '88');
INSERT INTO `UserInfo` VALUES ('87', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '89');
INSERT INTO `UserInfo` VALUES ('88', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '90');
INSERT INTO `UserInfo` VALUES ('89', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '91');
INSERT INTO `UserInfo` VALUES ('90', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '92');
INSERT INTO `UserInfo` VALUES ('91', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '93');
INSERT INTO `UserInfo` VALUES ('92', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '94');
INSERT INTO `UserInfo` VALUES ('93', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '95');
INSERT INTO `UserInfo` VALUES ('94', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '96');
INSERT INTO `UserInfo` VALUES ('95', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '97');
INSERT INTO `UserInfo` VALUES ('96', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '98');
INSERT INTO `UserInfo` VALUES ('97', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '99');
INSERT INTO `UserInfo` VALUES ('98', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '100');
INSERT INTO `UserInfo` VALUES ('99', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '101');
INSERT INTO `UserInfo` VALUES ('100', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '102');
INSERT INTO `UserInfo` VALUES ('101', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '103');
INSERT INTO `UserInfo` VALUES ('102', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '104');
INSERT INTO `UserInfo` VALUES ('103', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '105');
INSERT INTO `UserInfo` VALUES ('104', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '106');
INSERT INTO `UserInfo` VALUES ('105', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '107');
INSERT INTO `UserInfo` VALUES ('106', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '108');
INSERT INTO `UserInfo` VALUES ('107', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '109');
INSERT INTO `UserInfo` VALUES ('108', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '110');
INSERT INTO `UserInfo` VALUES ('109', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '111');
INSERT INTO `UserInfo` VALUES ('110', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '112');
INSERT INTO `UserInfo` VALUES ('111', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '113');
INSERT INTO `UserInfo` VALUES ('112', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '114');
INSERT INTO `UserInfo` VALUES ('113', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '115');
INSERT INTO `UserInfo` VALUES ('114', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '116');
INSERT INTO `UserInfo` VALUES ('115', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '117');
INSERT INTO `UserInfo` VALUES ('116', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '118');
INSERT INTO `UserInfo` VALUES ('117', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '119');
INSERT INTO `UserInfo` VALUES ('118', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '120');
INSERT INTO `UserInfo` VALUES ('119', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '121');
INSERT INTO `UserInfo` VALUES ('120', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '122');
INSERT INTO `UserInfo` VALUES ('121', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '123');
INSERT INTO `UserInfo` VALUES ('122', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '124');
INSERT INTO `UserInfo` VALUES ('123', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '125');
INSERT INTO `UserInfo` VALUES ('124', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '126');
INSERT INTO `UserInfo` VALUES ('125', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '127');
INSERT INTO `UserInfo` VALUES ('126', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '128');
INSERT INTO `UserInfo` VALUES ('127', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '129');
INSERT INTO `UserInfo` VALUES ('128', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '130');
INSERT INTO `UserInfo` VALUES ('129', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '131');
INSERT INTO `UserInfo` VALUES ('130', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '132');
INSERT INTO `UserInfo` VALUES ('131', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '133');
INSERT INTO `UserInfo` VALUES ('132', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '134');
INSERT INTO `UserInfo` VALUES ('133', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '135');
INSERT INTO `UserInfo` VALUES ('134', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '136');
INSERT INTO `UserInfo` VALUES ('135', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '137');
INSERT INTO `UserInfo` VALUES ('136', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '138');
INSERT INTO `UserInfo` VALUES ('137', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '139');
INSERT INTO `UserInfo` VALUES ('138', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '140');
INSERT INTO `UserInfo` VALUES ('139', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '141');
INSERT INTO `UserInfo` VALUES ('140', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '142');
INSERT INTO `UserInfo` VALUES ('141', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '143');
INSERT INTO `UserInfo` VALUES ('142', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '144');
INSERT INTO `UserInfo` VALUES ('143', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '145');
INSERT INTO `UserInfo` VALUES ('144', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '146');
INSERT INTO `UserInfo` VALUES ('145', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '147');
INSERT INTO `UserInfo` VALUES ('146', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '148');
INSERT INTO `UserInfo` VALUES ('147', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '149');
INSERT INTO `UserInfo` VALUES ('148', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '150');
INSERT INTO `UserInfo` VALUES ('149', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '151');
INSERT INTO `UserInfo` VALUES ('150', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '152');
INSERT INTO `UserInfo` VALUES ('151', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '153');
INSERT INTO `UserInfo` VALUES ('152', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '154');
INSERT INTO `UserInfo` VALUES ('153', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '155');
INSERT INTO `UserInfo` VALUES ('154', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '156');
INSERT INTO `UserInfo` VALUES ('155', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '157');
INSERT INTO `UserInfo` VALUES ('156', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '158');
INSERT INTO `UserInfo` VALUES ('157', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '159');
INSERT INTO `UserInfo` VALUES ('158', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '160');
INSERT INTO `UserInfo` VALUES ('159', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '161');
INSERT INTO `UserInfo` VALUES ('160', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '162');
INSERT INTO `UserInfo` VALUES ('161', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '163');
INSERT INTO `UserInfo` VALUES ('162', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '164');
INSERT INTO `UserInfo` VALUES ('163', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '165');
INSERT INTO `UserInfo` VALUES ('164', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '166');
INSERT INTO `UserInfo` VALUES ('165', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '167');
INSERT INTO `UserInfo` VALUES ('166', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '168');
INSERT INTO `UserInfo` VALUES ('167', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '169');
INSERT INTO `UserInfo` VALUES ('168', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '170');
INSERT INTO `UserInfo` VALUES ('169', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '171');
INSERT INTO `UserInfo` VALUES ('170', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '172');
INSERT INTO `UserInfo` VALUES ('171', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '173');
INSERT INTO `UserInfo` VALUES ('172', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '174');
INSERT INTO `UserInfo` VALUES ('173', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '175');
INSERT INTO `UserInfo` VALUES ('174', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '176');
INSERT INTO `UserInfo` VALUES ('175', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '177');
INSERT INTO `UserInfo` VALUES ('176', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '178');
INSERT INTO `UserInfo` VALUES ('177', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '179');
INSERT INTO `UserInfo` VALUES ('178', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '180');
INSERT INTO `UserInfo` VALUES ('179', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '181');
INSERT INTO `UserInfo` VALUES ('180', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '182');
INSERT INTO `UserInfo` VALUES ('181', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '183');
INSERT INTO `UserInfo` VALUES ('182', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '184');
INSERT INTO `UserInfo` VALUES ('183', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '185');
INSERT INTO `UserInfo` VALUES ('184', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '186');
INSERT INTO `UserInfo` VALUES ('185', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '187');
INSERT INTO `UserInfo` VALUES ('186', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '188');
INSERT INTO `UserInfo` VALUES ('187', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '189');
INSERT INTO `UserInfo` VALUES ('188', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '190');
INSERT INTO `UserInfo` VALUES ('189', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '191');
INSERT INTO `UserInfo` VALUES ('190', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '192');
INSERT INTO `UserInfo` VALUES ('191', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '193');
INSERT INTO `UserInfo` VALUES ('192', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '194');
INSERT INTO `UserInfo` VALUES ('193', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '195');
INSERT INTO `UserInfo` VALUES ('194', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '196');
INSERT INTO `UserInfo` VALUES ('195', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '197');
INSERT INTO `UserInfo` VALUES ('196', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '198');
INSERT INTO `UserInfo` VALUES ('197', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '199');
INSERT INTO `UserInfo` VALUES ('198', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '200');
INSERT INTO `UserInfo` VALUES ('199', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '201');
INSERT INTO `UserInfo` VALUES ('200', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '202');
INSERT INTO `UserInfo` VALUES ('201', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '203');
INSERT INTO `UserInfo` VALUES ('202', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '204');
INSERT INTO `UserInfo` VALUES ('203', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '205');
INSERT INTO `UserInfo` VALUES ('204', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '206');
INSERT INTO `UserInfo` VALUES ('205', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '207');
INSERT INTO `UserInfo` VALUES ('206', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '208');
INSERT INTO `UserInfo` VALUES ('207', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '1');
INSERT INTO `UserInfo` VALUES ('208', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '2');
INSERT INTO `UserInfo` VALUES ('209', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '3');
INSERT INTO `UserInfo` VALUES ('210', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '4');
INSERT INTO `UserInfo` VALUES ('211', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '5');
INSERT INTO `UserInfo` VALUES ('212', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '6');
INSERT INTO `UserInfo` VALUES ('213', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '7');
INSERT INTO `UserInfo` VALUES ('214', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '8');
INSERT INTO `UserInfo` VALUES ('215', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '9');
INSERT INTO `UserInfo` VALUES ('216', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '10');
INSERT INTO `UserInfo` VALUES ('217', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '12');
INSERT INTO `UserInfo` VALUES ('218', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '13');
INSERT INTO `UserInfo` VALUES ('219', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '14');
INSERT INTO `UserInfo` VALUES ('220', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '15');
INSERT INTO `UserInfo` VALUES ('221', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '16');
INSERT INTO `UserInfo` VALUES ('222', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '17');
INSERT INTO `UserInfo` VALUES ('223', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '18');
INSERT INTO `UserInfo` VALUES ('224', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '19');
INSERT INTO `UserInfo` VALUES ('225', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '20');
INSERT INTO `UserInfo` VALUES ('226', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '21');
INSERT INTO `UserInfo` VALUES ('227', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '22');
INSERT INTO `UserInfo` VALUES ('228', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '23');
INSERT INTO `UserInfo` VALUES ('229', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '24');
INSERT INTO `UserInfo` VALUES ('230', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '25');
INSERT INTO `UserInfo` VALUES ('231', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '26');
INSERT INTO `UserInfo` VALUES ('232', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '27');
INSERT INTO `UserInfo` VALUES ('233', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '28');
INSERT INTO `UserInfo` VALUES ('234', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '29');
INSERT INTO `UserInfo` VALUES ('235', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '30');
INSERT INTO `UserInfo` VALUES ('236', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '31');
INSERT INTO `UserInfo` VALUES ('237', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '32');
INSERT INTO `UserInfo` VALUES ('238', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '33');
INSERT INTO `UserInfo` VALUES ('239', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '34');
INSERT INTO `UserInfo` VALUES ('240', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '35');
INSERT INTO `UserInfo` VALUES ('241', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '36');
INSERT INTO `UserInfo` VALUES ('242', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '37');
INSERT INTO `UserInfo` VALUES ('243', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '38');
INSERT INTO `UserInfo` VALUES ('244', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '39');
INSERT INTO `UserInfo` VALUES ('245', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '40');
INSERT INTO `UserInfo` VALUES ('246', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '41');
INSERT INTO `UserInfo` VALUES ('247', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '42');
INSERT INTO `UserInfo` VALUES ('248', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '43');
INSERT INTO `UserInfo` VALUES ('249', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '44');
INSERT INTO `UserInfo` VALUES ('250', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '45');
INSERT INTO `UserInfo` VALUES ('251', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '46');
INSERT INTO `UserInfo` VALUES ('252', '', null, '0', '0', '0', '', '0', null, null, '0', '0', '47');

-- ----------------------------
-- Procedure structure for check_auth
-- ----------------------------
DROP PROCEDURE IF EXISTS `check_auth`;
DELIMITER ;;
CREATE DEFINER=`doosik`@`%` PROCEDURE `check_auth`(IN `in_token` varchar(80))
BEGIN
	#Routine body goes here...
	SELECT Auth.id FROM Auth WHERE Auth.token = in_token AND Auth.reg_date > NOW();
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for check_login
-- ----------------------------
DROP PROCEDURE IF EXISTS `check_login`;
DELIMITER ;;
CREATE DEFINER=`doosik`@`%` PROCEDURE `check_login`(IN `in_login` varchar(255))
BEGIN
	#Routine body goes here...
	SELECT Auth.id FROM Auth WHERE Auth.login = in_login;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for check_login_pass
-- ----------------------------
DROP PROCEDURE IF EXISTS `check_login_pass`;
DELIMITER ;;
CREATE DEFINER=`doosik`@`%` PROCEDURE `check_login_pass`(IN `in_login` varchar(255), IN `in_pass` varchar(255), IN `in_token_new` varchar(80))
BEGIN
	#Routine body goes here...
  UPDATE Auth SET Auth.token = in_token_new, Auth.reg_date = DATE_ADD(NOW(),INTERVAL 30 DAY) WHERE Auth.login = in_login AND Auth.`password` = in_pass AND NOW() > Auth.reg_date;
	SELECT Auth.id, Auth.token, Auth.reg_date, Auth.public_name FROM Auth WHERE Auth.login = in_login AND Auth.`password` = in_pass;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for register_new
-- ----------------------------
DROP PROCEDURE IF EXISTS `register_new`;
DELIMITER ;;
CREATE DEFINER=`doosik`@`%` PROCEDURE `register_new`(IN `in_login` varchar(100),IN `in_password` varchar(100), IN `in_name` varchar(100), IN `in_token` varchar(80), IN `in_date` varchar(25), OUT `out_last_id` int)
BEGIN
	#Routine body goes here...
INSERT INTO Auth (Auth.login, Auth.`name`, Auth.`password`, Auth.token, Auth.reg_date) 
VALUES (in_login, in_name, in_password, in_token, in_date);

SET out_last_id = LAST_INSERT_ID();
INSERT INTO UserInfo (UserInfo.id_reg) VALUES (out_last_id);

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for update_geo
-- ----------------------------
DROP PROCEDURE IF EXISTS `update_geo`;
DELIMITER ;;
CREATE DEFINER=`doosik`@`%` PROCEDURE `update_geo`(IN `in_token` varchar(50),IN `in_lat` double,IN `in_lng` double,IN `in_time` datetime)
BEGIN
	#Routine body goes here...
	UPDATE Auth SET Auth.latitude = in_lat, Auth.longitude = in_lng, Auth.geo_time = in_time WHERE Auth.token = in_token;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for update_token
-- ----------------------------
DROP PROCEDURE IF EXISTS `update_token`;
DELIMITER ;;
CREATE DEFINER=`doosik`@`%` PROCEDURE `update_token`(IN `in_login` varchar(255),IN `in_pass` varchar(255),IN `in_token` varchar(80),IN `in_date` varchar(25))
BEGIN
	#Routine body goes here...
  UPDATE Auth SET Auth.token = in_token, Auth.reg_date = in_date WHERE Auth.login = in_login AND Auth.`password` = in_pass;

END
;;
DELIMITER ;
