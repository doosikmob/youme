package com.youme.app.io.requests;

import com.android.volley.AuthFailureError;
import com.android.volley.RequestQueue;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.LoginEvent;
import com.youme.app.notify.Events;
import com.youme.app.notify.IdEvent;
import com.youme.app.utils.Settings;
import com.youme.app.utils.log;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * RegisterRequest
 */
public class LoginRequest extends BaseRequest {

    public static final String TAG_LOGIN = "tag_login";
    private String mLogin;
    private String mPassword;

    public static void newInstance(RequestQueue pRequestQueue, String pLogin, String pPassword) {
        LoginRequest request = new LoginRequest(pLogin, pPassword);
        request.setTag(TAG_LOGIN);
        pRequestQueue.cancelAll(TAG_LOGIN);
        pRequestQueue.add(request);
    }

    public LoginRequest(String pLogin, String pPassword) {
        super(Method.POST, "validate");
        mLogin = pLogin;
        mPassword = pPassword;
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        Map<String, String> headers = new HashMap<>();
        headers.put("login", mLogin);
        headers.put("password", mPassword);
        return headers;
    }

    @Override
    protected int getRetry() {
        return 0;
    }

    @Override
    protected void onResponse(JSONObject response) {
        Settings.get().setUserInfo(response);
        try {
            Answers.getInstance().logLogin(new LoginEvent()
                    .putMethod("Digits")
                    .putSuccess(true));

        } catch (Exception e) {
            log.e(e);
        }

        Events.post(new IdEvent(IdEvent.kLoginSucces));


    }
}
