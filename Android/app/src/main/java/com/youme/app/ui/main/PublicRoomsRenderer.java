package com.youme.app.ui.main;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.ViewGroup;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.google.maps.android.ui.IconGenerator;
import com.google.maps.android.ui.SquareTextView;
import com.youme.app.application.R;
import com.youme.app.entity.helper.ChatRoomWrapper;
import com.youme.app.ui.multichat.MultiChatFragment;
import com.youme.app.ui.utils.activitys.BaseActivity;
import com.youme.app.ui.utils.activitys.MainActivity;

/**
 * PublicRoomsRenderer
 */
public class PublicRoomsRenderer extends DefaultClusterRenderer<ChatRoomWrapper> {
    private final BaseActivity mActivity;
    private final IconGenerator mIconGenerator;
    private int mClasterColor, mClusterStroke;


    public PublicRoomsRenderer(BaseActivity pActivity, GoogleMap map, ClusterManager<ChatRoomWrapper> clusterManager) {
        super(pActivity, map, clusterManager);
        mActivity = pActivity;
        mClasterColor = ContextCompat.getColor(mActivity, R.color.colorAccent);
        mClusterStroke = ContextCompat.getColor(mActivity, android.R.color.white);
        mIconGenerator = new IconGenerator(mActivity);
        mIconGenerator.setContentView(makeSquareTextView(mActivity));
        mIconGenerator.setTextAppearance(R.style.ClusterIcon_TextAppearance);
        mIconGenerator.setBackground(makeClusterBackground());
    }



    @Override
    protected void onBeforeClusterRendered(Cluster<ChatRoomWrapper> cluster, MarkerOptions markerOptions) {
        int bucket = getBucket(cluster);
        BitmapDescriptor descriptor = BitmapDescriptorFactory.fromBitmap(mIconGenerator.makeIcon(getClusterText(bucket)));
        markerOptions.icon(descriptor);
    }

    @Override
    protected void onBeforeClusterItemRendered(ChatRoomWrapper item, MarkerOptions markerOptions) {
        markerOptions.position(item.position)
                .title(item.name)
                .snippet(item.owner)
                .icon(BitmapDescriptorFactory.fromResource(item.sycnronized ? R.drawable.ic_map_cloud_icon : R.drawable.ic_map_cloud_icon_disabled));
    }


    private LayerDrawable makeClusterBackground() {
        ShapeDrawable coloredCircleBackground = new ShapeDrawable(new OvalShape());
        coloredCircleBackground.getPaint().setColor(mClasterColor);
        ShapeDrawable outline = new ShapeDrawable(new OvalShape());
        outline.getPaint().setColor(mClusterStroke); // Transparent white.
        LayerDrawable mBackground = new LayerDrawable(new Drawable[]{outline, coloredCircleBackground});
        int mStrokeWidth = (int) (mActivity.getResources().getDisplayMetrics().density * 3);
        mBackground.setLayerInset(1, mStrokeWidth, mStrokeWidth, mStrokeWidth, mStrokeWidth);
        return mBackground;
    }

    private SquareTextView makeSquareTextView(Context context) {
        SquareTextView squareTextView = new SquareTextView(context);
        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        squareTextView.setLayoutParams(layoutParams);
        squareTextView.setId(R.id.text);

        int twelveDpi = (int) (12 * mActivity.getResources().getDisplayMetrics().density);
        squareTextView.setPadding(twelveDpi, twelveDpi, twelveDpi, twelveDpi);

        return squareTextView;
    }
}
