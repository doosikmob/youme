package com.youme.app.application;

import com.youme.app.utils.log;

/**
 * ErrorHandler
 */
public class ErrorHandler implements Thread.UncaughtExceptionHandler {


    private final Thread.UncaughtExceptionHandler mDeffaultHandler;

    public ErrorHandler(Thread.UncaughtExceptionHandler pDefaultUncaughtExceptionHandler) {
        super();
        mDeffaultHandler = pDefaultUncaughtExceptionHandler;
    }

    @Override
    public void uncaughtException(Thread thread, Throwable ex) {
        log.e(ex);
        Thread.setDefaultUncaughtExceptionHandler(mDeffaultHandler);
    }
}
