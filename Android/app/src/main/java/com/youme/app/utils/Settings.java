package com.youme.app.utils;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.util.TypedValue;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.youme.app.application.YMApplication;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Application settings
 */

public final class Settings {

    public static final  String HOST_SERVER   = "http://178.20.157.58:8080/v01/";
    public static final  String HOST_XMPP     = "178.20.157.58";
    private static final String TAG           = Settings.class.getSimpleName();
    public static final  String XMPP_RESOURCE = "Android";
    public static final  int    DISTANCE      = 10000;
    public static final int XMPP_PING_INTERVAL = 10000;


    private static Settings          sInstance;
    private final  SharedPreferences mPreferences;
    private final  SharedPreferences mPreferencesGlobal;


    public static final Settings get() {
        if (sInstance == null) {
            sInstance = new Settings(YMApplication.getContext());
        }
        return sInstance;
    }

    public Settings(Context pContext) {
        mPreferences = pContext.getSharedPreferences(pContext.getPackageName(), Context.MODE_PRIVATE);
        mPreferencesGlobal = pContext.getSharedPreferences(pContext.getPackageName() + "_global", Context.MODE_PRIVATE);
    }

    public static final class UserInfo {
        public static final String KEY_ACCESS_TOKEN = "access_token";
        public static final String KEY_USER_ID      = "userId";
        public static final String KEY_LOGIN_NAME   = "LoginName";
        public static final String KEY_PUBLIC_NAME  = "PublicName";
        public static final String KEY_JID          = "JID";
        public static final String KEY_JIDPassword  = "JIDPassword";
    }


    public SharedPreferences.Editor getEditor() {
        return mPreferences.edit();
    }

    public SharedPreferences.Editor getEditorGlobal() {
        return mPreferencesGlobal.edit();
    }


    public String getAccessToken() {
        return mPreferences.getString(UserInfo.KEY_ACCESS_TOKEN, null);
    }

    public String getUserId() {
        return mPreferences.getString(UserInfo.KEY_USER_ID, null);
    }

    public String getLoginName() {
        return mPreferences.getString(UserInfo.KEY_LOGIN_NAME, null);
    }

    public String getJID() {
        return mPreferences.getString(UserInfo.KEY_JID, null);
    }

    public String getPublicName() {
        return mPreferences.getString(UserInfo.KEY_PUBLIC_NAME, "unknown");
    }

    public String getJIDPass() {
        return mPreferences.getString(UserInfo.KEY_JIDPassword, null);
    }

    public synchronized void setUserInfo(JSONObject pJsonUserInfo) {
        try {
            Log.d(TAG, String.format("setUserInfo json :%s", pJsonUserInfo.toString()));
            SharedPreferences.Editor editor = getEditor();
            editor.putString(UserInfo.KEY_LOGIN_NAME, pJsonUserInfo.getString("UserName"));
            editor.putString(UserInfo.KEY_ACCESS_TOKEN, pJsonUserInfo.getString("Token"));
            editor.putString(UserInfo.KEY_JID, pJsonUserInfo.getString("JID"));
            editor.putString(UserInfo.KEY_PUBLIC_NAME, pJsonUserInfo.getString("PublicName"));
            editor.putString(UserInfo.KEY_JIDPassword, pJsonUserInfo.getString("ChatPassword"));
            editor.putInt(UserInfo.KEY_USER_ID, pJsonUserInfo.getInt("UserId"));
            editor.commit();
        } catch (JSONException e) {
            log.e(e);
        }


//        Realm realm = Realm.getDefaultInstance();
//        try {
//            UserHelper userHelper = new UserHelper(realm);
//
//            realm.beginTransaction();
//            User myUser = userHelper.getMyUser();
//            if (myUser == null) {
//                myUser = userHelper.createOrUpdate();
//                myUser.setId(get().getUserId());
//            }
//            myUser.setName(get().getPublicName());
//
//            realm.commitTransaction();
//        } catch (Exception e) {
//            realm.cancelTransaction();
//            log.w("setUserInfo", e);
//        } finally {
//            realm.close();
//        }
    }


    public synchronized void clear() {
        SharedPreferences.Editor editor = getEditor();
        editor.clear();
        editor.commit();
    }

    public boolean isTokenValid() {
        return mPreferences.getString(UserInfo.KEY_ACCESS_TOKEN, null) != null;
    }

    public void invalidateToken() {
        mPreferences.edit()
                .remove(UserInfo.KEY_ACCESS_TOKEN)
                .remove(UserInfo.KEY_USER_ID)
                .remove(UserInfo.KEY_LOGIN_NAME)
                .remove(UserInfo.KEY_PUBLIC_NAME)
                .remove(UserInfo.KEY_JID)
                .remove(UserInfo.KEY_JIDPassword)
                .apply();
    }


    public String getOnlyJidUser() {
        return getJID().substring(0, getJID().indexOf("@"));
    }

    public boolean isMyJidConference(String from) {
        return Pattern.matches(String.format("^.*@.*\\/%s*_.*$", getOnlyJidUser()), from);
    }

    public static String getPublicNameFromMessage(String from) {

        Pattern pattern = Pattern.compile("^.*@.*\\/u_[0-9]*_(.*)$");
        Matcher matcher = pattern.matcher(from);
        if (matcher.matches()) {
            return matcher.group(1);
        }

        return "Unknown";
    }

    public static boolean isGooglePlayServicesAvailable(Context ctx) {
        return ConnectionResult.SUCCESS == resultGooglePlayServices(ctx);
    }

    public static int resultGooglePlayServices(Context ctx) {
        return GooglePlayServicesUtil.isGooglePlayServicesAvailable(ctx);
    }

    public static int getActionBarDeffaultSize(Context pContext) {
        TypedValue tv = new TypedValue();
        if (pContext.getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
            return TypedValue.complexToDimensionPixelSize(tv.data, pContext.getResources().getDisplayMetrics());
        }

        return 0;
    }

    public static Date getLastDate(Context pContext) {
        LocationManager locMan = (LocationManager) pContext.getSystemService(Context.LOCATION_SERVICE);

        if (ActivityCompat.checkSelfPermission(pContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(pContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return null;
        }
        return new Date(locMan.getLastKnownLocation(LocationManager.NETWORK_PROVIDER).getTime());
    }


    public static boolean testping(){
        int exit=22;
        Process p;

        try {
            p = Runtime.getRuntime().exec("ping -c1 -W1 " + Settings.HOST_XMPP);
            //make shure that is -W not -w it's not he same.
            p.waitFor();
            exit = p.exitValue();
            p.destroy();
        } catch (IOException ee) {
            log.w(ee);
        } catch (InterruptedException e) {
            log.w(e);
        }
        if (exit == 0) {
            return true;
        }else{
            return false;
        }
    }

}
