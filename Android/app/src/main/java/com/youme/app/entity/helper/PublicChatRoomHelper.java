package com.youme.app.entity.helper;

import android.text.TextUtils;
import android.util.Log;

import com.youme.app.entity.PublicChatRoom;
import com.youme.app.utils.Settings;
import com.youme.app.utils.StringUtil;
import com.youme.app.utils.log;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.exceptions.RealmException;

/**
 * PublicChatRoomHelper
 */
public class PublicChatRoomHelper extends BaseHelper<PublicChatRoom> {


    public PublicChatRoomHelper(Realm pRealm) {
        super(pRealm, PublicChatRoom.class);
    }



    public void createOrUpdateToRealm(String pJid, String pName, int pDistance, double pLat, double pLng, String pHash, long pDate, String pOwner, boolean isSync) {

        PublicChatRoom room = getByHash(pHash);
        if (room == null && !pJid.equals(pHash) && isSync) {
            room = getByJid(pJid);
        }
        try {

            beginTransaction();
            if (room == null || !room.isValid()) {
                room = getRealm().createObject(PublicChatRoom.class);
                room.setHash(pHash);
            }
            room.setId(pJid);
            room.setName(pName);
            room.setDistance(pDistance);
            room.setLat(pLat);
            room.setLng(pLng);
            room.setOwner(pOwner);
            room.setCreation(pDate);
            room.setSycnronized(isSync);

            commitTransaction();
        } catch (RealmException e) {
            log.e(e);
            cancelTransaction();
        }

    }

    public PublicChatRoom getByHash(String hash) {
        return query().equalTo("hash", hash).findFirst();
    }

    public PublicChatRoom getByJid(String pJid) {
        return query().equalTo("id", pJid).findFirst();
    }

    public List<ChatRoomWrapper> getRooms() {
        List<ChatRoomWrapper> list = new ArrayList<>();
        for (PublicChatRoom room : findAll()) {
            list.add(new ChatRoomWrapper(room));
        }
        return list;
    }

    public String getGlobalHash() {
        String hash = "";
        for (PublicChatRoom room : findAll()) {
            hash += room.getHash();
            hash += String.valueOf(room.isSycnronized());
        }
        return hash.equals("") ? "" :StringUtil.md5(hash);
    }
}
