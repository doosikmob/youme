package com.youme.app.throwable;

import android.support.v4.content.ContextCompat;

import com.youme.app.application.R;
import com.youme.app.application.YMApplication;

/**
 * EErrorMessage
 */
public enum EErrorMessage {
    E0(0, R.string.error0, android.R.color.holo_red_dark),
    EL1(-1, R.string.error_l_1, android.R.color.holo_red_dark),
    E404(404, R.string.error404, android.R.color.holo_red_dark);

    private final int mStatusCode;
    private final int mMessage;
    private final int mColor;

    EErrorMessage(int pStatusCode, int pMessage, int pColor) {
        mStatusCode = pStatusCode;
        mMessage = pMessage;
        mColor = pColor;
    }

    public int getStatusCode() {
        return mStatusCode;
    }

    public int getMessage() {
        return mMessage;
    }

    public String getMessageString() {
        return YMApplication.getContext().getResources().getString(mMessage);
    }

    public int getColor() {
        return ContextCompat.getColor(YMApplication.getContext(), mColor);
    }



    public static EErrorMessage getByStatusCode(int pStatusCode) {
        for (EErrorMessage eErrorMessage : values()) {
            if (eErrorMessage.getStatusCode() == pStatusCode) {
                return eErrorMessage;
            }
        }
        return E0;
    }
}
