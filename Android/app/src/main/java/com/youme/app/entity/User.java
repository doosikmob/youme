package com.youme.app.entity;

import android.location.Location;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * User
 */
public class User extends RealmObject {

	@PrimaryKey
	private String id;

	private String name;
	private boolean vip;
	private boolean banned;
	private boolean myban;
	private double lat;
	private double lng;
	private Date lastloc;
	private int role;
	private String phone;
	private int age;
	private int sex;
	private int rating;
	private String photo;


	public int getAge() {
		return age;
	}

	public void setAge(int pAge) {
		age = pAge;
	}

	public boolean isBanned() {
		return banned;
	}

	public void setBanned(boolean pBanned) {
		banned = pBanned;
	}

	public String getId() {
		return id;
	}

	public void setId(String pId) {
		id = pId;
	}

	public Date getLastloc() {
		return lastloc;
	}

	public void setLastloc(Date pLastloc) {
		lastloc = pLastloc;
	}

	public double getLat() {
		return lat;
	}

	public void setLat(double pLat) {
		lat = pLat;
	}

	public double getLng() {
		return lng;
	}

	public void setLng(double pLng) {
		lng = pLng;
	}

	public boolean isMyban() {
		return myban;
	}

	public void setMyban(boolean pMyban) {
		myban = pMyban;
	}

	public String getName() {
		return name;
	}

	public void setName(String pName) {
		name = pName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String pPhone) {
		phone = pPhone;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String pPhoto) {
		photo = pPhoto;
	}

	public int getRating() {
		return rating;
	}

	public void setRating(int pRating) {
		rating = pRating;
	}

	public int getRole() {
		return role;
	}

	public void setRole(int pRole) {
		role = pRole;
	}

	public int getSex() {
		return sex;
	}

	public void setSex(int pSex) {
		sex = pSex;
	}

	public boolean isVip() {
		return vip;
	}

	public void setVip(boolean pVip) {
		vip = pVip;
	}
}
