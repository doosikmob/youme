package com.youme.app.io.requests;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.SignUpEvent;
import com.youme.app.notify.Events;
import com.youme.app.notify.IdEvent;
import com.youme.app.utils.Settings;
import com.youme.app.utils.log;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * RegisterRequest
 */
public class RegisterRequest extends BaseRequest {

    public static final String TAG_REGISTER = "tag_register";
    private final String mName;
    private String mLogin;
    private String mPassword;

    public static void newInstance(RequestQueue pRequestQueue, String pLogin, String pPassword, String pName) {
        RegisterRequest request = new RegisterRequest(pLogin, pPassword, pName);
        request.setTag(TAG_REGISTER);
        pRequestQueue.cancelAll(TAG_REGISTER);
        pRequestQueue.add(request);
    }


    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        Map<String, String> headers = new HashMap<>();
        headers.put("login", mLogin);
        headers.put("password", mPassword);
        headers.put("public_name", mName);
        return headers;
    }

    public RegisterRequest(String pLogin, String pPassword, String pName) {
        super(Method.POST, "register");
        mLogin = pLogin;
        mPassword = pPassword;
        mName = pName;
    }

    @Override
    protected int getRetry() {
        return 0;
    }

    @Override
    protected void onResponse(JSONObject response) {
        if (response != null) {

            try {
                Answers.getInstance().logSignUp(new SignUpEvent()
                        .putMethod("Digits")
                        .putSuccess(true));

            } catch (Exception e) {
                log.e(e);
            }

            Settings.get().setUserInfo(response);
            Events.post(new IdEvent(IdEvent.kRegisterSucces));
        }

    }
}
