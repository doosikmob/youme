package com.youme.app.ui.main;

import android.graphics.Point;
import android.location.Location;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.maps.android.clustering.ClusterManager;
import com.youme.app.DraggedImageView;
import com.youme.app.application.R;
import com.youme.app.application.YMApplication;
import com.youme.app.entity.helper.ChatRoomWrapper;
import com.youme.app.entity.helper.PublicChatRoomHelper;
import com.youme.app.ui.multichat.MultiChatFragment;
import com.youme.app.ui.utils.activitys.BaseActivity;
import com.youme.app.ui.utils.activitys.MainActivity;
import com.youme.app.utils.log;

import io.realm.RealmChangeListener;

/**
 * MainMapFragment
 */

public class MainMapFragment extends MapFragment implements
        OnMapReadyCallback,
        DraggedImageView.OnDragged,
        RealmChangeListener,
        ClusterManager.OnClusterItemInfoWindowClickListener<ChatRoomWrapper> {
    private GoogleMap map;
    private View mMapView;
    private DraggedImageView mMultiChatIcon;
    private ImageView mMultiChatIconDiasabled;
    private ClusterManager<ChatRoomWrapper> mMapManager;
    private PublicChatRoomHelper mChatRoomHelper;
    private String mStoredHash;
    private PublicRoomsRenderer mMapRenderer;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mMapView =  super.onCreateView(inflater, container, savedInstanceState);
        RelativeLayout parentView  = (RelativeLayout) inflater.inflate(R.layout.fragment_map, container, false);
        RelativeLayout mapContainer = (RelativeLayout) parentView.findViewById(R.id.rl_map_container);
        mapContainer.addView(mMapView);
        mMultiChatIcon = (DraggedImageView) parentView.findViewById(R.id.iv_chat_room_container);
        mMultiChatIconDiasabled = (ImageView) parentView.findViewById(R.id.iv_chat_room_container_disabled);
        mMultiChatIcon.setOnDraggedListner(this);

        return parentView;
    }


    @Override
    public void onPause() {
        super.onPause();
        ((BaseActivity) getActivity()).getRealm().removeChangeListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        mStoredHash = "";
        getMapAsync(this);
        ((BaseActivity) getActivity()).getRealm().addChangeListener(this);
        mChatRoomHelper = new PublicChatRoomHelper(((BaseActivity) getActivity()).getRealm());
    }


    @Override
    public void onMapReady(GoogleMap pGoogleMap) {
        map = pGoogleMap;
        map.setMyLocationEnabled(true);

        mMapManager = new ClusterManager<ChatRoomWrapper>(getActivity(), getMap());
        mMapRenderer = new PublicRoomsRenderer((BaseActivity) getActivity(), getMap(), mMapManager);

        map.setOnCameraChangeListener(mMapManager);
        map.setOnMarkerClickListener(mMapManager);
        map.setOnInfoWindowClickListener(mMapManager);

        mMapManager.setRenderer(mMapRenderer);
        mMapManager.setOnClusterItemInfoWindowClickListener(this);



        updateClouds();

        Location location = YMApplication.getCurrentLocation();
        if (location == null) return;

        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 16));


    }

    @Override
    public void OnDragStart() {
        mMultiChatIconDiasabled.setVisibility(View.VISIBLE);
    }

    @Override
    public void OnDragEnd(int pX, int pY) {
        LatLng newpoint = map.getProjection().fromScreenLocation(new Point(pX, pY));
        DialogNewCloud.newInstance(newpoint).show(getFragmentManager(), DialogNewCloud.class.getName());
        mMultiChatIconDiasabled.setVisibility(View.GONE);
        updateClouds();
    }

    @Override
    public void onChange() {
        updateClouds();
    }

    private void updateClouds() {
        if (mMapManager != null && mChatRoomHelper != null && !mStoredHash.equals(mChatRoomHelper.getGlobalHash())) {
            mMapManager.clearItems();
            mMapManager.addItems(mChatRoomHelper.getRooms());
            mMapManager.cluster();
        }
    }


    @Override
    public void onClusterItemInfoWindowClick(ChatRoomWrapper pChatRoomWrapper) {
        if (pChatRoomWrapper.sycnronized) {
            Bundle bundle = new Bundle();
            bundle.putString(MultiChatFragment.ROOM_JID, pChatRoomWrapper.id);
            ((MainActivity) getActivity()).showPresentations(MultiChatFragment.class.getName(), false, bundle);
        }
    }

}
