package com.youme.app.ui.multichat;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;

import com.youme.app.application.R;
import com.youme.app.chat.YMMultiChat;
import com.youme.app.entity.helper.ChatMessageHelper;
import com.youme.app.ui.utils.activitys.BaseActivity;
import com.youme.app.ui.utils.fragments.BaseFragment;

/**
 * MultiChatFragment
 */
public class MultiChatFragment extends BaseFragment implements View.OnClickListener {

    public static final String ROOM_JID = "RoomJID";
    private ListView mListChatRoom;
    private YMMultiChat mChat;
    private EditText mChatEdit;
    private ChatMessageHelper mMessageHelper;
    private String mRoomJid;


    @Override
    public void onDestroyView() {
        if (mChat != null) {
            mChat.disconnect();
        }

        super.onDestroyView();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle arg = getArguments();

        if (arg != null && arg.containsKey(ROOM_JID)) {
            mRoomJid = arg.getString(ROOM_JID);
        } else {
            mRoomJid = "doosik@conference.178.20.157.58";
        }
        mChat = new YMMultiChat(mRoomJid, ((BaseActivity) getActivity()).getRealm());

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mMessageHelper = new ChatMessageHelper(((BaseActivity) getActivity()).getRealm());
        mListChatRoom.setAdapter(new MultiChatAdapter(getActivity(), mMessageHelper.findSortedByJid(mRoomJid), null));
}

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View parent = inflater.inflate(R.layout.fragment_multi_chat, container, false);
        mListChatRoom = (ListView) parent.findViewById(R.id.list_multi_chat);
        mChatEdit = (EditText) parent.findViewById(R.id.et_post_message);
        parent.findViewById(R.id.button_post_message).setOnClickListener(this);
        return parent;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_post_message:
                String postMessage = mChatEdit.getText().toString();
                if (!TextUtils.isEmpty(postMessage)) {
                    mChat.sendMessage(postMessage);
                    mChatEdit.getText().clear();

                }
                break;
        }
    }
}
