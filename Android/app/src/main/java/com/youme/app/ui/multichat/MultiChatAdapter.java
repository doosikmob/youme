package com.youme.app.ui.multichat;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.youme.app.application.R;
import com.youme.app.entity.ChatMessage;
import com.youme.app.ui.utils.OnAdapterClickListener;
import com.youme.app.ui.utils.adapters.BaseHolderAdapter;
import com.youme.app.utils.Settings;
import com.youme.app.utils.log;

import java.text.SimpleDateFormat;
import java.util.Date;

import io.realm.RealmResults;

/**
 * MultiChatAdapter.
 */
public class MultiChatAdapter extends BaseHolderAdapter<ChatMessage, MultiChatAdapter.ChatMessageHolder>{


    private final SimpleDateFormat mSF;
    private final int mMyColor, mNotMyColor;

    public MultiChatAdapter(Context context, RealmResults<ChatMessage> realmResults, OnAdapterClickListener<ChatMessage> pAdapterClickListener) {
        super(context, realmResults, pAdapterClickListener);
        mSF = new SimpleDateFormat();
        mMyColor = ContextCompat.getColor(context, R.color.myMessageColor);
        mNotMyColor = ContextCompat.getColor(context, R.color.colorPrimary);


    }

    @Override
    protected int getViewType(ChatMessage pEntity) {
        String from = !TextUtils.isEmpty(pEntity.getFrom()) ? pEntity.getFrom() : "server";
        return Settings.get().isMyJidConference(from)  || !pEntity.isSyncronized() ? 1 : 0;
    }

    @Override
    public ChatMessageHolder onCreateViewHolder(LayoutInflater inflater, ViewGroup parent, int pViewType) {
        return new ChatMessageHolder(inflater.inflate(pViewType == 1 ? R.layout.view_message_my : R.layout.view_message, parent, false), pViewType);
    }

    @Override
    public void onBindViewHolder(ChatMessageHolder holder, ChatMessage entity) {
        String date =  mSF.format(entity.getCreated() != null ? entity.getCreated() : new Date());
        String from = !TextUtils.isEmpty(entity.getFrom()) ? entity.getFrom() : "server";
        holder.date.setText(date);
        holder.from.setText(!entity.isSyncronized() ? Settings.get().getPublicName() : Settings.getPublicNameFromMessage(from));
        holder.message.setText(entity.getBody());
        boolean isMy = Settings.get().isMyJidConference(from) || !entity.isSyncronized();
        holder.message.setTextColor(isMy ? mMyColor : mNotMyColor);
        holder.rootView.setAlpha(entity.isSyncronized() ? 1.0f : 0.6f);
    }

    class ChatMessageHolder extends BaseHolderAdapter.ViewHolder{

        public final TextView message, date, from;
        public final View rootView;

        public ChatMessageHolder(View pView, int pViewType) {
            super(pView, pViewType);
            message = (TextView) findViewById(R.id.tv_message);
            date = (TextView) findViewById(R.id.tv_date);
            from = (TextView) findViewById(R.id.tv_jid_from);
            rootView = pView;

        }


    }


}
