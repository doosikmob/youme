package com.youme.app.ui.main;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.youme.app.application.R;
import com.youme.app.application.YMApplication;
import com.youme.app.entity.PublicChatRoom;
import com.youme.app.entity.helper.PublicChatRoomHelper;
import com.youme.app.io.requests.AddCloudRequest;
import com.youme.app.ui.utils.activitys.BaseActivity;
import com.youme.app.utils.Settings;
import com.youme.app.utils.StringUtil;
import com.youme.app.utils.log;

/**
 * NewCloudDialog
 */
public class DialogNewCloud extends DialogFragment {


    private static final String LAT = "NewCloudDialog_latitude";
    private static final String LNG = "NewCloudDialog_longitude";
    private LatLng latLng;

    public static DialogNewCloud newInstance(LatLng latLng) {

        Bundle args = new Bundle();
        args.putDouble(LAT, latLng.latitude);
        args.putDouble(LNG, latLng.longitude);
        DialogNewCloud fragment = new DialogNewCloud();
        fragment.setArguments(args);
        return fragment;
    }


    private AutoCompleteTextView mEditText;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        latLng = new LatLng(getArguments().getDouble(LAT), getArguments().getDouble(LNG));

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.title_dialog_newCloud);
        builder.setView(initView());
        builder.setPositiveButton(R.string.button_ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String owner = Settings.get().getJID();

                String hash = StringUtil.md5(
                        String.valueOf(System.currentTimeMillis()),
                        mEditText.getText().toString().trim(),
                        owner,
                        latLng.toString()
                );

                PublicChatRoomHelper roomHelper = new PublicChatRoomHelper(((BaseActivity) getActivity()).getRealm());
                roomHelper.createOrUpdateToRealm(
                        owner,
                        mEditText.getText().toString().trim(),
                        Settings.DISTANCE,
                        latLng.latitude,
                        latLng.longitude,
                        hash,
                        System.currentTimeMillis(),
                        Settings.get().getJID(),
                        false
                );

                AddCloudRequest.newInstance(
                        (((BaseActivity) getActivity()).getApp()).getRequestQueue(),
                        Settings.get().getAccessToken(),
                        hash);

                dismiss();

            }
        });
        builder.setNegativeButton(R.string.button_dissmis, null);
        return builder.create();

    }

    private View initView() {
        LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View parent = layoutInflater.inflate(R.layout.dialog_new_cloud, null);
        mEditText = (AutoCompleteTextView) parent.findViewById(R.id.dialog_edittext);
        return parent;
    }
}
