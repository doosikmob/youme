package com.youme.app.utils;

import android.util.Log;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.youme.app.application.YMApplication;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * log
 */

public class log {
    private static final String TAG = "ARoglyph";

    private static final boolean LOCATION_ENABLED = true;


    private static String sDeviceId;


    public static void log(final int priority, final String tag, final String msg, final Throwable tr) {
        String message;
        if (tr == null) {
            message = msg != null ? msg + " " + getLocation() : getLocation();
        } else {
            message = msg != null ? msg + "\n" + Log.getStackTraceString(tr) : Log.getStackTraceString(tr);
        }
        Log.println(priority, tag, message);
    }


// ================== TAG logging ===================== //

    public static void vt(String tag, String format, Object... args) {
        log(Log.VERBOSE, tag, String.format(format, args), null);
    }

    public static void dt(String tag, String format, Object... args) {
        log(Log.DEBUG, tag, String.format(format, args), null);
    }

    public static void it(String tag, String format, Object... args) {
        log(Log.INFO, tag, String.format(format, args), null);
    }


    public static void wt(String tag, Throwable tr) {
        log(Log.WARN, tag, null, tr);
    }

    public static void wt(String tag, String msg) {
        log(Log.WARN, tag, msg, null);
    }

    public static void wt(String tag, String msg, Throwable tr) {
        log(Log.WARN, tag, msg, tr);
    }


    public static void et(String tag, Throwable tr) {
        log(Log.ERROR, tag, null, tr);
    }

    public static void et(String tag, String msg) {
        log(Log.ERROR, tag, msg, null);
    }

    public static void et(String tag, String msg, Throwable tr) {
        log(Log.ERROR, tag, msg, tr);
    }


// ================== Not TAG logging ===================== //

    public static void v(String format, Object... args) {
        log(Log.VERBOSE, TAG, String.format(format, args), null);
    }

    public static void d(String format, Object... args) {
        log(Log.DEBUG, TAG, String.format(format, args), null);
    }

    public static void i(String format, Object... args) {
        log(Log.INFO, TAG, String.format(format, args), null);
    }


    public static void w(String format, Object... args) {
        log(Log.WARN, TAG, String.format(format, args), null);
    }

    public static void w(Throwable tr) {
        log(Log.WARN, TAG, null, tr);
    }


    public static void e(Throwable tr) {
        logsNalitics();
        log(Log.ERROR, TAG, tr.getMessage(), tr);
    }

    public static void logsNalitics() {
        Crashlytics.setUserIdentifier("4234242");
        Crashlytics.setUserEmail("doosikmob@gmail.com");
        Crashlytics.setUserName("doosik");
    }


    public static void e(String msg) {
        logsNalitics();
        log(Log.ERROR, TAG, msg, null);
    }

    public static void e(String msg, Throwable tr) {
        logsNalitics();
        log(Log.ERROR, TAG, msg, tr);
    }


    private static String getLocation() {
        if (!LOCATION_ENABLED) return "";

        final String logClassName = log.class.getName();
        final StackTraceElement[] traces = Thread.currentThread().getStackTrace();
        boolean found = false;

        for (int i = 0; i < traces.length; i++) {
            StackTraceElement trace = traces[i];

            if (found) {
                if (!trace.getClassName().startsWith(logClassName)) {
                    return String.format("[%s(%s:%d)]", trace.getMethodName(), trace.getFileName(), trace.getLineNumber());
                }
            } else if (trace.getClassName().startsWith(logClassName)) {
                found = true;
            }

        }
        return "";
    }

    public static void t (Object... pObj) {
        String log_string = "";
        int i = 0;
        for (Object string : pObj) {
            if (i > 0) {
                log_string += " : ";

            }
            log_string += String.valueOf(string);
            i++;
        }

        Toast.makeText(YMApplication.getContext(), log_string, Toast.LENGTH_LONG).show();
    }
}