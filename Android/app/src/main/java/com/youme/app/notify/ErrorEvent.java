package com.youme.app.notify;

/**
 * ErrorEvent
 */
public class ErrorEvent {

    private final int mStatusCode;
    private final String mMessage;
    private final int mColor;
    private final String mException;


    public ErrorEvent(int pStatusCode, String pMessage, int pColor, String pException) {
        mStatusCode = pStatusCode;
        mMessage = pMessage;
        mColor = pColor;
        mException = pException;
    }


    public int getStatusCode() {
        return mStatusCode;
    }

    public String getMessage() {
        return mMessage;
    }

    public int getColor() {
        return mColor;
    }

    public String getException() {
        return mException;
    }

}
