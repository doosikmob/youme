package com.youme.app.entity;

import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;

/**
 * PublicChatRoom
 */
public class PublicChatRoom extends RealmObject {
    @PrimaryKey
    private String hash;
    private String id;
    private String name;
    private int distance;
    private double lat;
    private double lng;
    private String owner;
    private long creation;
    private boolean sycnronized;

    @Ignore
    private boolean position;

    public long getCreation() {
        return creation;
    }

    public void setCreation(long pCreation) {
        creation = pCreation;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int pDistance) {
        distance = pDistance;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String pHash) {
        hash = pHash;
    }

    public String getId() {
        return id;
    }

    public void setId(String pId) {
        id = pId;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double pLat) {
        lat = pLat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double pLng) {
        lng = pLng;
    }

    public String getName() {
        return name;
    }

    public void setName(String pName) {
        name = pName;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String pOwner) {
        owner = pOwner;
    }

    public boolean isSycnronized() {
        return sycnronized;
    }

    public void setSycnronized(boolean pSycnronized) {
        sycnronized = pSycnronized;
    }



    //	//Ignored
//	public boolean isInSet() {
//		Location location = new Location("PublicChat");
//		location.setLatitude(lat);
//		location.setLongitude(lng);
//		return distance == 0 || (YMApplication.getCurrentLocation() != null ? YMApplication.getCurrentLocation().distanceTo(location) <= distance : false);
//	}
//
//	public void setIsInSet(boolean pIsInSet) {
//		//isInSet = pIsInSet;
//	}

}
