package com.youme.app.ui.drawer;


import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.TextView;

import com.youme.app.application.R;
import com.youme.app.ui.main.MainMapFragment;
import com.youme.app.ui.multichat.MultiChatFragment;
import com.youme.app.ui.utils.activitys.MainActivity;
import com.youme.app.ui.utils.fragments.BaseFragment;


public class NavigationDrawerFragment extends BaseFragment implements View.OnClickListener {


	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View parent = inflater.inflate(R.layout.drawer_main, container, false);

		parent.findViewById(R.id.txt_navigation_map).setOnClickListener(this);
		parent.findViewById(R.id.txt_navigation_friend).setOnClickListener(this);
		parent.findViewById(R.id.txt_navigation_public_rooms).setOnClickListener(this);
		parent.findViewById(R.id.txt_navigation_harem).setOnClickListener(this);
		parent.findViewById(R.id.txt_navigation_games).setOnClickListener(this);
		parent.findViewById(R.id.txt_navigation_bank).setOnClickListener(this);
		parent.findViewById(R.id.txt_navigation_logout).setOnClickListener(this);
		return parent;
	}


	@Override
	public void onClick(View v) {
		MainActivity activity = (MainActivity) getActivity();

		switch (v.getId()) {
			case R.id.txt_navigation_map:
				activity.showPresentations(MainMapFragment.class.getName(), false);
				break;
			case R.id.txt_navigation_friend:
				break;
			case R.id.txt_navigation_public_rooms:
				activity.showPresentations(MultiChatFragment.class.getName(), false);
				break;
			case R.id.txt_navigation_harem:
				break;
			case R.id.txt_navigation_games:
				break;
			case R.id.txt_navigation_bank:
				break;
			case R.id.txt_navigation_logout:
				break;
		}
	}
}
