package com.youme.app.ui.utils;


import android.view.View;

import io.realm.RealmObject;

/**
 * OnAdapterClickListener
 */
public interface OnAdapterClickListener<T extends RealmObject> {

    void onClickAdapter(int pViewId, T entity, View view);

}