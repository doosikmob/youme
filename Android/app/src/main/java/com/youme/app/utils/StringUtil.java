package com.youme.app.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * StringUtil
 */
public class StringUtil {


	private static final String TAG = StringUtil.class.getSimpleName();

	public static final String md5(String... pSrings) {
		StringBuilder sepValueBuilder = new StringBuilder();

		//Looping through the list
		for (int i = 0; i < pSrings.length; i++) {
			sepValueBuilder.append(pSrings[i]);
			if (i != pSrings.length - 1) {
				sepValueBuilder.append("_");
			}
		}
		return md5(sepValueBuilder.toString());
	}

	public static final String md5(final String pString) {
		final String MD5 = "MD5";
		try {
			// Create MD5 Hash
			MessageDigest digest = java.security.MessageDigest
					.getInstance(MD5);
			digest.update(pString.getBytes());
			byte messageDigest[] = digest.digest();

			// Create Hex String
			StringBuilder hexString = new StringBuilder();
			for (byte aMessageDigest : messageDigest) {
				String h = Integer.toHexString(0xFF & aMessageDigest);
				while (h.length() < 2)
					h = "0" + h;
				hexString.append(h);
			}
			return hexString.toString();

		} catch (NoSuchAlgorithmException e) {
			log.et(TAG, e);
		}
		return "";
	}
}
