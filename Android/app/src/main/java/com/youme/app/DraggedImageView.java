package com.youme.app;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.youme.app.utils.Settings;

/**
 * DraggedImageView
 */
public class DraggedImageView extends ImageView implements OnTouchListener {


    private ViewGroup mRootLayout;

    private OnDragged mOnDragged;
    private int _xDelta;
    private int _yDelta;

    public DraggedImageView(Context context) {
        this(context, null);
    }

    public DraggedImageView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public DraggedImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setOnTouchListener(this);

    }


    public void setOnDraggedListner(OnDragged pOnDragged) {
        mOnDragged = pOnDragged;
    }



    @Override
    public boolean onTouch(View v, MotionEvent event) {
        final int X = (int) event.getRawX();
        final int Y = (int) event.getRawY();
        switch (event.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN:
                mRootLayout = (ViewGroup) getParent();
                RelativeLayout.LayoutParams lParams = (RelativeLayout.LayoutParams) v.getLayoutParams();
                _xDelta = X - lParams.leftMargin;
                _yDelta = Y - lParams.topMargin;
                if (mOnDragged != null) {
                    mOnDragged.OnDragStart();
                }
                break;
            case MotionEvent.ACTION_UP:
                if (mOnDragged != null) {
                    int[] loc = new int[2];
                    v.getLocationOnScreen(loc);
                    int _x = loc[0] + v.getWidth() / 2;
                    int _y = loc[1] + v.getHeight()   -  (int)(v.getPaddingBottom() * 2.5) - Settings.getActionBarDeffaultSize(getContext());
                    mOnDragged.OnDragEnd( _x, _y);
                }

                returnToMain(v);


                break;
            case MotionEvent.ACTION_POINTER_DOWN:
                break;
            case MotionEvent.ACTION_POINTER_UP:
                break;
            case MotionEvent.ACTION_MOVE:
                RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) v.getLayoutParams();
                layoutParams.leftMargin = X - _xDelta;
                layoutParams.topMargin = Y - _yDelta;
                layoutParams.rightMargin = _xDelta - X;
                layoutParams.bottomMargin = _yDelta - Y;
                v.setLayoutParams(layoutParams);
                break;
        }
        mRootLayout.invalidate();
        return true;
    }

    private void returnToMain(View v) {
        RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) v.getLayoutParams();
        lp.leftMargin = 0;
        lp.topMargin = 0;
        lp.rightMargin = 0;
        lp.bottomMargin = 0;
        v.setLayoutParams(lp);
    }


    private int getRelativeLeft(View myView) {
        if (myView.getParent() == myView.getRootView())
            return myView.getLeft();
        else
            return myView.getLeft() + getRelativeLeft((View) myView.getParent());
    }

    private int getRelativeTop(View myView) {
        if (myView.getParent() == myView.getRootView())
            return myView.getTop();
        else
            return myView.getTop() + getRelativeTop((View) myView.getParent());
    }

    public interface OnDragged {
        void OnDragStart();
        void OnDragEnd(int pX, int pY);
    }
}
