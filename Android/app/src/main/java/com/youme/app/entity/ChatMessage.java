package com.youme.app.entity;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * ChatMessage
 */
public class ChatMessage extends RealmObject {

    @PrimaryKey
    private long id;
    private String messageId;
    private Date created;
    private String roomJid;
    private String body;
    private String from;
    private String to;
    private String hash;
    private boolean syncronized;



    public long getId() {
        return id;
    }

    public void setId(long pId) {
        id = pId;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String pMessageId) {
        messageId = pMessageId;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date pCreated) {
        created = pCreated;
    }

    public String getRoomJid() {
        return roomJid;
    }

    public void setRoomJid(String pRoomJid) {
        roomJid = pRoomJid;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String pBody) {
        body = pBody;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String pFrom) {
        from = pFrom;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String pTo) {
        to = pTo;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String pHash) {
        hash = pHash;
    }

    public boolean isSyncronized() {
        return syncronized;
    }

    public void setSyncronized(boolean pSyncronized) {
        syncronized = pSyncronized;
    }
}
