package com.youme.app.ui.login;


import android.app.FragmentTransaction;
import android.content.Intent;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.widget.Toast;

import com.squareup.otto.Subscribe;
import com.youme.app.application.R;
import com.youme.app.notify.ErrorEvent;
import com.youme.app.notify.Events;
import com.youme.app.ui.utils.activitys.BaseActivity;
import com.youme.app.ui.utils.activitys.MainActivity;
import com.youme.app.utils.Settings;

import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;

public class LoginActivity extends BaseActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_content_without_toolbar);


    }

    public void startMain() {
        Intent intent = new Intent(this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }


    @Override
    protected void onResume() {
        super.onResume();
        Events.register(this);
        if (Settings.get().isTokenValid()) {
            startMain();
        } else {
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.replace(R.id.content, new LoginFragment());
            ft.commit();
        }
    }

    @Override
    protected void onPause() {
        Events.unregister(this);
        super.onPause();
    }



    @Subscribe
    public void onErrorEvent(ErrorEvent pErrorEvent) {
        if (pErrorEvent != null) {
            String message = pErrorEvent.getMessage();
            Crouton.makeText(this, String.format("Status: %d, Error: %s", pErrorEvent.getStatusCode(), TextUtils.isEmpty(message) ? "not set" : message), Style.ALERT).show();
        }
    }





}






