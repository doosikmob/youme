package com.youme.app.entity.helper;

import com.youme.app.io.requests.BaseRequest;
import com.youme.app.throwable.EErrorMessage;

import io.realm.Realm;

/**
 * RealmExt
 */
public class RealmExt {
    public static void post(OnRealm pOnRealm) {
        Realm realm = Realm.getDefaultInstance();
        if (realm != null && pOnRealm != null) {
            try {
                pOnRealm.onRealm(realm);
            } catch (Exception e) {
                BaseRequest.postError(EErrorMessage.E0, e.getLocalizedMessage());
            } finally {
                if (realm != null) {
                    realm.close();
                }
            }
        }
    }

    ;

    public interface OnRealm {
        void onRealm(Realm pRealm);
    }
}
