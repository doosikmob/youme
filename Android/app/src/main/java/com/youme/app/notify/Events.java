package com.youme.app.notify;



import com.squareup.otto.Bus;
import com.youme.app.utils.log;

/**
 * Events
 */

public class Events {
    private static final String TAG = "EventsBus";
    private static final Bus BUS = new ThreadSafeBus();

    public static void register(Object object) {
        try {
            BUS.register(object);
        } catch (Exception e){
            log.et(TAG, "ERROR register", e);
        }
    }

    public static void unregister(Object object) {
        try {
            BUS.unregister(object);
        } catch (Exception e){
            log.et(TAG, "ERROR unregister", e);
        }
    }

    public static void post(Object event) {
        BUS.post(event);
    }
}
