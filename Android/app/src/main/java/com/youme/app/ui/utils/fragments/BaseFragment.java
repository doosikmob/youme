package com.youme.app.ui.utils.fragments;

import android.app.Fragment;
import android.os.Bundle;


import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.ContentViewEvent;
import com.youme.app.application.YMApplication;
import com.youme.app.notify.Events;

/**
 * BaseFragment
 */
public class BaseFragment extends Fragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Answers.getInstance().logContentView(new ContentViewEvent());
    }

    public YMApplication getApp() {
        if (getActivity() != null && isAdded()) {
            return (YMApplication) getActivity().getApplication();
        }
        return null;
    }
}
