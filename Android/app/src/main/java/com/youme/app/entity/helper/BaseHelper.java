package com.youme.app.entity.helper;

import android.support.annotation.Nullable;

import com.youme.app.entity.helper.SyncState;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.exceptions.RealmException;

/**
 * BaseHelper
 */
public abstract class BaseHelper<T extends RealmObject> {
    protected final Realm mRealm;
    protected final Class<T> mClazz;

    public BaseHelper(Realm pRealm, Class<T> pClazz) {
        mRealm = pRealm;
        mClazz = pClazz;
    }

    public Realm getRealm() {
        return mRealm;
    }

    protected void setInternalId(T pEntity) {
        //Override this is needed.
    }

    /**
     * "Transparent" transaction operations
     * Only for compact code
     */
    public void beginTransaction(){
        mRealm.beginTransaction();
    }

    public void commitTransaction(){
        mRealm.commitTransaction();
    }

    public void cancelTransaction(){
        mRealm.cancelTransaction();
    }

    /**
     * Возвращает сущность из базы по СЕРВЕРНОМУ ID
     * @param entityId Серверный ID
     * @return сущность
     */
    @Nullable
    public T getEntityById(long entityId) {
        return query().equalTo("id", entityId).findFirst();
    }

    /**
     * Возвращает сущность из базы по СЕРВЕРНОМУ ID
     * @param pId Серверный ID
     * @return сущность
     */
    @Nullable
    public T getEntityById(String pId) {
        return query().equalTo("Id", pId).findFirst();
    }

    public long count() {
        return query().count();
    }

    /**
     * Create entity and set internal identificator
     */
    public T create() {
        T entity = mRealm.createObject(mClazz);
        setInternalId(entity);
        return entity;
    }

    public void delete(T pEntity) {
        pEntity.removeFromRealm();
    }

    public void delete(List<T> pEntitys) {
        final Object[] pEntityArray = pEntitys.toArray();
        delete(pEntityArray);
    }

    public void delete(Object[] pEntityArray) {
        for (Object anAll : pEntityArray) {
            T item = (T) anAll;
            item.removeFromRealm();
        }
    }

    public void deleteAll() {
        mRealm.clear(mClazz);
    }

    /**
     * Return all entities in realm with same type T
     */
    public RealmResults<T> findAll() {
        return mRealm.allObjects(mClazz);
    }

    public RealmQuery<T> query() {
        return mRealm.where(mClazz);
    }


    public T createOrUpdateFromJson(JSONObject pJson) throws RealmException, JSONException {
        return mRealm.createOrUpdateObjectFromJson(mClazz, pJson);
    }

    public List<T> createOrUpdateAllFromJson(JSONArray pJson) throws JSONException, RealmException {
        final int length = pJson.length();
        List<T> result = new ArrayList<>(length);

        for (int i = 0; i < length; i++) {
            T entity = createOrUpdateFromJson(pJson.getJSONObject(i));
            if (entity != null) {
                result.add(entity);
            }
        }

        return result;
    }

//    public JSONObject getJson(T pEntity) throws JSONException {
//        return null;
//    }
//
//    public String getJsonString(T pEntity) throws JSONException {
//        return getJson(pEntity).toString();
//    }

    public RealmResults<T> allBySyncState(SyncState state) {
        return query().equalTo("SyncState", state.ordinal()).findAll();
    }



}
