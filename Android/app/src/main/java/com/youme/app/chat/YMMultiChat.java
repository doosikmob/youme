package com.youme.app.chat;

import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;

import com.youme.app.entity.ChatMessage;
import com.youme.app.entity.helper.ChatMessageHelper;
import com.youme.app.entity.helper.RealmExt;
import com.youme.app.io.requests.BaseRequest;
import com.youme.app.throwable.EErrorMessage;
import com.youme.app.utils.Settings;
import com.youme.app.utils.log;

import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.ConnectionListener;
import org.jivesoftware.smack.MessageListener;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.StanzaListener;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.filter.StanzaFilter;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Stanza;
import org.jivesoftware.smack.provider.ProviderManager;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smack.tcp.XMPPTCPConnectionConfiguration;
import org.jivesoftware.smackx.delay.packet.DelayInformation;
import org.jivesoftware.smackx.delay.provider.LegacyDelayInformationProvider;
import org.jivesoftware.smackx.muc.DiscussionHistory;
import org.jivesoftware.smackx.muc.MultiUserChat;
import org.jivesoftware.smackx.muc.MultiUserChatManager;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * @author doosik
 * YMMultiChat
 */
public class YMMultiChat implements StanzaFilter, StanzaListener, ConnectionListener, MessageListener {
    private static final String TAG = YMMultiChat.class.getSimpleName();

    private final XMPPTCPConnection mChatConnection;
    private final String mJid;
    private final Runnable mRunnublePinger;
    private final Handler mHandle;
    private MultiUserChat mChat;
    private Realm mRealm;

    public YMMultiChat(String pJid, Realm pRealm) {
        mRealm = pRealm;
        mJid = pJid;
        XMPPTCPConnectionConfiguration connectionConfiguration = XMPPTCPConnectionConfiguration
                .builder()
                .setCompressionEnabled(false)
                .setConnectTimeout(10000)
                .setHost(Settings.HOST_XMPP)
                .setServiceName(Settings.HOST_XMPP)
                .setResource(Settings.get().getPublicName())
                .setPort(5222)
                .setSecurityMode(ConnectionConfiguration.SecurityMode.disabled)
                .setSendPresence(true)
                .setUsernameAndPassword(Settings.get().getJID(), Settings.get().getJIDPass())
                .build();


        mHandle = new Handler();
        mRunnublePinger = new Runnable() {
            @Override
            public void run() {
                try {
                    // BaseRequest.postError(EErrorMessage.E0, "start relogin");
                    if (Settings.testping()) {
                        BaseRequest.postError(EErrorMessage.E0, "start relogin");
                        relogin();
                    } else {
                        mHandle.postDelayed(mRunnublePinger, 5000);
                    }
                } catch (Exception e) {
                    BaseRequest.postError(EErrorMessage.E0, e.getLocalizedMessage());
                    mHandle.postDelayed(mRunnublePinger, 5000);
                }

            }
        };

        mChatConnection = new XMPPTCPConnection(connectionConfiguration);
        mChatConnection.addSyncStanzaListener(this, this);
        mChatConnection.addConnectionListener(this);

        relogin();
        // mChatConnection.

    }

    private void relogin() {
        new LoginXMPPP().execute();
    }

    @Override
    public boolean accept(Stanza stanza) {
        Log.d(TAG, String.format("accept: %s", stanza.toXML()));
        return true;
    }

    @Override
    public void processPacket(Stanza packet) throws SmackException.NotConnectedException {
        DelayInformation delay = packet.getExtension("x", "jabber:x:delay");
        if (delay != null)
            return; //Discard this packet
        delay = packet.getExtension("x", "urn:xmpp:delay");
        if (delay != null)
            return;
        Log.d(TAG, String.format("Stanza: %s", packet.toXML()));
    }

    public void disconnect() {
        if (mHandle != null && mRunnublePinger != null) {
            mHandle.removeCallbacks(mRunnublePinger);
        }

        if (mChat != null && mChat.isJoined()) {
            try {
                mChat.leave();
            } catch (SmackException.NotConnectedException e) {
                log.et(TAG, e);
            }
        }

        mChatConnection.disconnect();
    }

    @Override
    public void connected(XMPPConnection connection) {
        Log.d(TAG, String.format("connected: %s", connection.getStreamId()));
    }

    @Override
    public void authenticated(XMPPConnection connection, boolean resumed) {
        Log.d(TAG, String.format("authenticated: resumed %s : %b", connection.getStreamId(), resumed));
    }

    @Override
    public void connectionClosed() {
        Log.d(TAG, "connectionClosed");
    }

    @Override
    public void connectionClosedOnError(Exception e) {
        Log.d(TAG, String.format("connectionClosedError: %s", e.getMessage()));
        try {
            mChatConnection.disconnect();
        } catch (Exception ex) {
            BaseRequest.postError(EErrorMessage.E0, ex.getLocalizedMessage());
        }

        if (mHandle != null && mRunnublePinger != null) {
            mHandle.removeCallbacks(mRunnublePinger);
            mHandle.postDelayed(mRunnublePinger, Settings.XMPP_PING_INTERVAL);
        }

    }

    @Override
    public void reconnectionSuccessful() {
        Log.d(TAG, "reconnectionSuccessful");
    }

    @Override
    public void reconnectingIn(int seconds) {
        Log.d(TAG, String.format("reconnectingIn: %d", seconds));
    }

    @Override
    public void reconnectionFailed(Exception e) {
        Log.d(TAG, String.format("reconnectingIn: %s", e.getMessage()));
    }

    @Override
    public void processMessage(final Message message) {
        Date dt = new Date();
        if (message.hasExtension("urn:xmpp:delay")) {
            DelayInformation dates = (DelayInformation) message.getExtension("urn:xmpp:delay");
            dt = dates.getStamp();
        }

        final Date dts = dt;
        createMessage(message, dts, true);

        Log.d(TAG, String.format("message: %s : %s", new SimpleDateFormat("MM/DD HH:mm:ss", Locale.getDefault()).format(dt), message.getBody()));//, message.getThread(), message.hashCode(), message.getStanzaId(), message.getFrom(), message.toXML()));

    }

    private void createMessage(final Message message, final Date pDts, final boolean isSync) {
        Log.d("_createMessage", message.toXML().toString());
        RealmExt.post(new RealmExt.OnRealm() {
            @Override
            public void onRealm(Realm pRealm) {
                // Log.d("_createMessage", message.toXML().toString());
                new ChatMessageHelper(pRealm).createOrUpdate(message, pDts, mJid, isSync);
            }
        });
    }


    public void sendMessage(String pMessage) {

        Message message = new Message(mJid, pMessage);

        log.dt(TAG, String.format("sendMessage: joined: %b, message: %s, stanza: %s", mChat.isJoined(), pMessage, message.getStanzaId() == null ? "null" : message.getStanzaId()));

        try {
            createMessage(message, new Date(), false);
            if (mChatConnection.isConnected() && mChat.isJoined()) {
                mChat.sendMessage(message);
            }


        } catch (SmackException.NotConnectedException e) {
            log.et(TAG, e);
        }
    }


    class LoginXMPPP extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            ProviderManager.addExtensionProvider("x", "jabber:x:delay", new LegacyDelayInformationProvider());
            MultiUserChatManager chatmanager = MultiUserChatManager.getInstanceFor(mChatConnection);

            mChat = chatmanager.getMultiUserChat(mJid);

            DiscussionHistory history = new DiscussionHistory();
            history.setMaxStanzas(25);
            try {
                mChat.join(mChatConnection.getUser().substring(0, mChatConnection.getUser().indexOf("@")) + "_" + Settings.get().getPublicName(), Settings.get().getJIDPass(), history, 10000);
            } catch (Exception e) {
                log.e(e);
            }

            mChat.addMessageListener(YMMultiChat.this);


            sendOldMessages();

        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                mChatConnection.connect();

                mChatConnection.login(Settings.get().getJID(), Settings.get().getJIDPass());
            } catch (Exception e) {
                log.e(e);
            }

            return null;
        }


    }

    private void sendOldMessages() {
        if (mRealm != null) {
            RealmResults<ChatMessage> listNotSync = new ChatMessageHelper(mRealm).findSortedNotSync(mJid);
            for (int i = 0; i < listNotSync.size(); i++) {
                ChatMessage chatMessage = listNotSync.get(i);
                if (
                        chatMessage != null
                                && chatMessage.isValid()
                                && mChatConnection.isConnected()
                                && mChat.isJoined()) {
                    try {
                        Message message = new Message(mJid, chatMessage.getBody());
                        message.setStanzaId(chatMessage.getMessageId());
                        message.setTo(chatMessage.getTo());

                        mChat.sendMessage(message);
                    } catch (SmackException.NotConnectedException e) {
                        log.et(TAG, e);
                    }
                }

            }
        }
    }


}
