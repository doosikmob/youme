package com.youme.app.entity.helper;

/**
 * Created by doosik on 19.12.2015.
 */
public enum SyncState {
    SYNCHRONIZED,
    NEW,
    UPDATE,
    DELETE;

    public static SyncState get(int pOrdinal) {
        if (pOrdinal < 0 || pOrdinal >= SyncState.values().length) {
            return SyncState.SYNCHRONIZED;
        }
        return SyncState.values()[pOrdinal];
    }
}