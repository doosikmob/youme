package com.youme.app.ui.login;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;

import com.squareup.otto.Subscribe;
import com.youme.app.application.R;
import com.youme.app.io.requests.LoginRequest;
import com.youme.app.notify.ErrorEvent;
import com.youme.app.notify.Events;
import com.youme.app.notify.IdEvent;
import com.youme.app.ui.utils.fragments.BaseFragment;

import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;

/**
 * RegisterFragment.
 */
public class LoginFragment extends BaseFragment implements View.OnClickListener {

    private AutoCompleteTextView mLoginEdit;

    private EditText mPasswordEdit;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View parent = inflater.inflate(R.layout.fragment_login, container, false);
        mLoginEdit = (AutoCompleteTextView) parent.findViewById(R.id.email);
        mPasswordEdit = (EditText) parent.findViewById(R.id.password);
        parent.findViewById(R.id.email_sign_in_button).setOnClickListener(this);
        parent.findViewById(R.id.register_new).setOnClickListener(this);
        return parent;
    }

    @Override
    public void onClick(View pView) {
        switch (pView.getId()) {
            case R.id.email_sign_in_button:
                String login = mLoginEdit.getText().toString();
                String password = mPasswordEdit.getText().toString();
                LoginRequest.newInstance(getApp().getRequestQueue(), login, password);

                break;
            case R.id.register_new:
                getActivity().getFragmentManager().beginTransaction()
                        .replace(R.id.content, new RegisterFragment())
                        .addToBackStack(this.getClass().getName())
                        .commit();
        }

    }

    @Subscribe
    public void onEventId(IdEvent pEvent) {
        if (pEvent != null && pEvent.id == IdEvent.kLoginSucces) {
            ((LoginActivity) getActivity()).startMain();
        }
    }

    @Subscribe
    public void onErrorEvent(ErrorEvent pErrorEvent) {
        if (pErrorEvent != null) {
            String message = pErrorEvent.getMessage();
            Crouton.makeText(getActivity(), String.format("Status: %d, Error: %s", pErrorEvent.getStatusCode(), TextUtils.isEmpty(message) ? "not set" : message), Style.ALERT).show();
        }
    }



    @Override
    public void onPause() {
        Events.unregister(this);
        super.onDestroy();
    }

    @Override
    public void onResume() {
        super.onStart();
        Events.register(this);
    }



}
