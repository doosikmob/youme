package com.youme.app.ui.login;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.ContentViewEvent;
import com.squareup.otto.Subscribe;
import com.youme.app.application.R;
import com.youme.app.io.requests.RegisterRequest;
import com.youme.app.notify.Events;
import com.youme.app.notify.IdEvent;
import com.youme.app.ui.utils.fragments.BaseFragment;

/**
 * LoginFragment
 */
public class RegisterFragment extends BaseFragment implements View.OnClickListener {

    private AutoCompleteTextView mLoginEdit;
    private Button mSubmitButton;
    private EditText mPasswordEdit;
    private RequestQueue mRequestQueue;
    private EditText mNameEdit;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View parent = inflater.inflate(R.layout.fragment_register, container, false);
        mLoginEdit = (AutoCompleteTextView) parent.findViewById(R.id.email);
        mPasswordEdit = (EditText) parent.findViewById(R.id.password);
        mNameEdit = (EditText)parent.findViewById(R.id.public_name);
        mSubmitButton = (Button) parent.findViewById(R.id.email_sign_in_button);

        mSubmitButton.setOnClickListener(this);
        return parent;
    }

    @Override
    public void onClick(View pView) {
        switch (pView.getId()) {
            case R.id.email_sign_in_button:
                String login = mLoginEdit.getText().toString();
                String password = mPasswordEdit.getText().toString();
                String public_name = mNameEdit.getText().toString();
                RegisterRequest.newInstance(getApp().getRequestQueue(), login, password, public_name);
                break;

        }

    }

    @Subscribe
    public void onEventId(IdEvent pEvent) {
        if (pEvent != null && pEvent.id == IdEvent.kRegisterSucces) {
            ((LoginActivity) getActivity()).startMain();
        }
    }


    @Override
    public void onPause() {
        Events.unregister(this);
        super.onDestroy();
    }

    @Override
    public void onResume() {
        super.onStart();
        Events.register(this);
    }

}
