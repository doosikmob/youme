package com.youme.app.notify;

/**
 * IdEvent
 */

public class IdEvent {
    public static final int kRegisterSucces =     0xA1;
    public static final int kLoginSucces =        0xA2;

    public final int id;
    public final Throwable throwable;

    public IdEvent(int id) {
        this.id = id;
        this.throwable = null;
    }

    public IdEvent(int id, Throwable throwable) {
        this.id = id;
        this.throwable = throwable;
    }
}
