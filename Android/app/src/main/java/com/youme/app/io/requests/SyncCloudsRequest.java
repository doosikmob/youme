package com.youme.app.io.requests;

import android.location.Location;

import com.android.volley.AuthFailureError;
import com.android.volley.RequestQueue;
import com.google.android.gms.maps.model.LatLng;
import com.youme.app.entity.helper.PublicChatRoomHelper;
import com.youme.app.entity.helper.RealmExt;
import com.youme.app.utils.Settings;
import com.youme.app.utils.StringUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import io.realm.Realm;

/**
 * SyckRoomRequest
 */
public class SyncCloudsRequest extends BaseRequest {

    public static final String TAG_SYNC_CLOUD = "tag_sync_cloud";
    private final Location mLocation;
    private String mToken;


    public static void newInstance(RequestQueue pRequestQueue, String pToken, Location pLocation) {
        SyncCloudsRequest request = new SyncCloudsRequest(pToken, pLocation);
        request.setTag(TAG_SYNC_CLOUD);
        pRequestQueue.cancelAll(TAG_SYNC_CLOUD);
        pRequestQueue.add(request);
    }

    public SyncCloudsRequest(String pToken, Location pLocation) {
        super(Method.POST, "sync_rooms");
        mToken = pToken;
        mLocation = pLocation;
    }


    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        Map<String, String> headers = new HashMap<>();
        headers.put("token", mToken);
        headers.put("lat", String.valueOf(mLocation.getLatitude()));
        headers.put("lng", String.valueOf(mLocation.getLongitude()));
        return headers;
    }



    @Override
    protected int getRetry() {
        return 0;
    }

    @Override
    protected void onResponse(JSONObject response) throws JSONException {
        JSONArray roomsArray = response.getJSONArray("public_rooms");
        for (int i = 0; i < roomsArray.length() ; i++) {
           JSONObject jsonObject = roomsArray.getJSONObject(i);
           final String jid = jsonObject.getString("jid");
           final String name = jsonObject.getString("name");
           final double center_lat = jsonObject.getDouble("center_lat");
           final double center_lng = jsonObject.getDouble("center_lng");
           final String owner = String.format("u_%s@%s", jsonObject.getString("owner_id"), Settings.HOST_XMPP);
           final long creation = jsonObject.getLong("creation");
           String public_name = jsonObject.has("public_name") ? jsonObject.getString("public_name") : "udenfined";
           final int distance = Settings.DISTANCE;
           final String hash = StringUtil.md5(
                   String.valueOf(creation),
                   name,
                   owner,
                   new LatLng(center_lat, center_lng).toString()
           );

           RealmExt.post(new RealmExt.OnRealm() {
               @Override
               public void onRealm(Realm pRealm) {
                   PublicChatRoomHelper helper = new PublicChatRoomHelper(pRealm);
                   helper.createOrUpdateToRealm(jid, name, distance, center_lat, center_lng, hash, creation, owner, true);
               }
           });


        }
    }
}
