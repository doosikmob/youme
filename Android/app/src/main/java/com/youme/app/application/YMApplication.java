package com.youme.app.application;

import android.app.Application;
import android.content.Context;
import android.location.Location;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.crashlytics.android.Crashlytics;
import com.youme.app.location.YMLocation;

import io.fabric.sdk.android.Fabric;
import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * YMApplication
 */
public class YMApplication extends Application {
    private static Context sContext;


    private RequestQueue                    mRequestQueue;
    private static YMLocation               ymLocation;


    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        sContext = getApplicationContext();


        mRequestQueue = Volley.newRequestQueue(this);

        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder(this)
                .schemaVersion(1)
                .migration(new Migration())
                        //.deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(realmConfiguration);

        ymLocation = new YMLocation();
        ymLocation.connect(this);


    }


	public static Location getCurrentLocation() {
		if (ymLocation != null) {
            return ymLocation.getLocation();
        } else {
            return null;
        }
	}

	public static Context getContext() {
        return sContext;
    }

    public RequestQueue getRequestQueue() {
        return mRequestQueue;
    }

}
