package com.youme.app.entity.helper;

import com.youme.app.entity.User;

import io.realm.Realm;

/**
 * UserHelper
 */
public class UserHelper extends BaseHelper<User> {

	public UserHelper(Realm pRealm) {
		super(pRealm, User.class);
	}
}
