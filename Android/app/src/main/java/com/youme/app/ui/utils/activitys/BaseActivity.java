package com.youme.app.ui.utils.activitys;

import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.widget.Toast;

import com.squareup.otto.Subscribe;
import com.youme.app.application.YMApplication;
import com.youme.app.notify.ErrorEvent;
import com.youme.app.notify.Events;

import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;
import io.realm.Realm;

public class BaseActivity extends AppCompatActivity implements
        ActivityCompat.OnRequestPermissionsResultCallback {

    protected final String TAG = BaseActivity.class.getSimpleName();
    private Realm mRealm;



    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mRealm = Realm.getDefaultInstance();


    }




    @Override
    protected void onDestroy() {
        if (mRealm != null) {
            mRealm.close();
            mRealm = null;
        }
        super.onDestroy();
    }


    /**
     * @throws ClassCastException
     */
    public YMApplication getApp() {
        return (YMApplication) this.getApplication();
    }


    public Realm getRealm() {
        return mRealm;
    }

}
