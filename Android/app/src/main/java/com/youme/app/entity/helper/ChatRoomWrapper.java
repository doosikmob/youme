package com.youme.app.entity.helper;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;
import com.youme.app.entity.PublicChatRoom;

/**
 * ChatRoomWrapper
 */
public class ChatRoomWrapper implements ClusterItem {
    public final String hash;
    public final String id;
    public final String name;
    public final int distance;
    public final String owner;
    public final long creation;
    public final boolean sycnronized;
    public final LatLng position;



    public ChatRoomWrapper(final PublicChatRoom pChatRoom) {
        id = pChatRoom.getId();
        hash = pChatRoom.getHash();
        distance = pChatRoom.getDistance();
        name = pChatRoom.getName();
        owner = pChatRoom.getOwner();
        creation = pChatRoom.getCreation();
        sycnronized = pChatRoom.isSycnronized();
        position = new LatLng(pChatRoom.getLat(), pChatRoom.getLng());
    }



    @Override
    public boolean equals(Object obj) {
        return ChatRoomWrapper.class.isInstance(obj) && ((ChatRoomWrapper)obj).hash == hash;
    }

    @Override
    public LatLng getPosition() {
        return position;
    }




}
