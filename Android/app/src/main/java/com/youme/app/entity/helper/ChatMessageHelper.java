package com.youme.app.entity.helper;

import com.youme.app.entity.ChatMessage;
import com.youme.app.utils.Settings;
import com.youme.app.utils.StringUtil;
import com.youme.app.utils.log;

import org.jivesoftware.smack.packet.Message;

import java.util.Date;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.exceptions.RealmException;

/**
 * ChatMessageHelper
 */
public class ChatMessageHelper extends BaseHelper<ChatMessage> {
    public ChatMessageHelper(Realm pRealm) {
        super(pRealm, ChatMessage.class);
    }

    @Override
    protected void setInternalId(ChatMessage pEntity) {
        pEntity.setId(query().maximumInt("id") + 1);
    }

    public void createOrUpdate(Message pMessage, Date pTimeCreated, String pRoomJID, boolean isSync) {
        String messageId = pMessage.getStanzaId();
        String body = pMessage.getBody();
        String from = isSync ? pMessage.getFrom() : pRoomJID + "/" + Settings.get().getJID().substring(0, Settings.get().getJID().indexOf("@"));
        String to = pMessage.getTo();
        String hash = ChatMessageHelper.getHash(pMessage, pRoomJID);

       try {
            beginTransaction();
            ChatMessage chatMessage = getByHash(hash);
            if (chatMessage == null) {
                chatMessage = new ChatMessage();
                setInternalId(chatMessage);
            }

            chatMessage.setMessageId(messageId);
            chatMessage.setCreated(pTimeCreated);
            chatMessage.setRoomJid(pRoomJID);
            chatMessage.setBody(body == null ? "" : body);
            chatMessage.setFrom(from == null ? "" : from);
            chatMessage.setTo(to == null ? "" :  to);
            chatMessage.setHash(hash);
            chatMessage.setSyncronized(isSync);
            getRealm().copyToRealm(chatMessage);

            commitTransaction();
        } catch (RealmException e) {
            log.e(e);
            cancelTransaction();
        }
    }

    public ChatMessage getByHash(String hash) {
        return query().equalTo("hash", hash).findFirst();
    }


    public boolean isExists(String hash) {
        return query().equalTo("hash", hash).findAll().size() > 0;
    }

    public RealmResults<ChatMessage> findSortedByJid(String pJid) {
        return query().equalTo("roomJid", pJid).findAllSorted("created", true);
    }

    public RealmResults<ChatMessage> findSortedNotSync(String pJid) {
        return query().equalTo("roomJid", pJid).equalTo("syncronized", false).findAllSorted("created", true);
    }


    public static String getHash (Message pMessage, String pJid) {
       return StringUtil.md5(pMessage.getStanzaId(), pJid, pMessage.getBody());
    }
}
