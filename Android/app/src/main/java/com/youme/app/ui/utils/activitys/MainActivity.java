package com.youme.app.ui.utils.activitys;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;

import com.squareup.otto.Subscribe;
import com.youme.app.application.R;
import com.youme.app.application.YMApplication;
import com.youme.app.entity.ChatMessage;
import com.youme.app.entity.PublicChatRoom;
import com.youme.app.entity.User;
import com.youme.app.io.requests.SyncCloudsRequest;
import com.youme.app.notify.ErrorEvent;
import com.youme.app.notify.Events;
import com.youme.app.ui.drawer.NavigationDrawerFragment;
import com.youme.app.ui.main.MainMapFragment;
import com.youme.app.utils.Settings;

import org.jivesoftware.smack.chat.Chat;

import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;
import io.realm.Realm;


public class MainActivity extends BaseActivity {


    private static final String TAG            = MainActivity.class.getSimpleName();
    public static final  String FRAGMENT_CLAZZ = "FragmentClazz";
    private final String mMainClass = MainMapFragment.class.getName();


    private NavigationDrawerFragment mNavigationDrawerFragment;

    private CharSequence mTitle;
    private Toolbar      mToolBar;
    private String       mCurrentFragment;
    private String       mArgs;
    private DrawerLayout mDrawerLayout;
    private Handler      mHandler;
    private Runnable     mRunnuble;
    private ViewGroup mViewCrouton;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_exit:
                Settings.get().invalidateToken();
                try {
                    getRealm().beginTransaction();
                    getRealm().clear(User.class);
                    getRealm().clear(PublicChatRoom.class);
                    getRealm().clear(ChatMessage.class);
                    getRealm().commitTransaction();
                } catch (Exception e) {
                    getRealm().cancelTransaction();
                }

                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mArgs = "";
        mTitle = getTitle();

        mToolBar = (Toolbar) findViewById(R.id.toolbar);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mViewCrouton = (ViewGroup) findViewById(R.id.crouton);
        setSupportActionBar(mToolBar);

        if (savedInstanceState != null && savedInstanceState.containsKey(FRAGMENT_CLAZZ)) {
            showPresentations(savedInstanceState.getString(FRAGMENT_CLAZZ), false);
        } else {
            showPresentations(mMainClass, false);
        }

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getFragmentManager().findFragmentById(R.id.navigation_drawer);


    }

    @Override
    public void onBackPressed() {
        if (mMainClass.equals(mCurrentFragment)) {
            super.onBackPressed();
        } else {
            showPresentations(mMainClass, false);
        }
    }

    private void createSyncOpperation() {
        mHandler = new Handler();
        mRunnuble = new Runnable() {
            @Override
            public void run() {
                sentSync();
                mHandler.postDelayed(mRunnuble, 60000); //min
            }
        };
        mHandler.post(mRunnuble);

    }

    private void sentSync() {
        Location location = YMApplication.getCurrentLocation();
        if (location != null) {
            SyncCloudsRequest.newInstance(getApp().getRequestQueue(), Settings.get().getAccessToken(), location);
        }
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mCurrentFragment != null) {
            outState.putString(FRAGMENT_CLAZZ, mCurrentFragment);
        }

    }


    public void showPresentations(String classname, boolean pPopBackStack){
        showPresentations(classname, pPopBackStack, null);
    }


    public void showPresentations(String classname, boolean pPopBackStack, Bundle pArgs) {
        String compareArgs = pArgs == null ? "" : pArgs.toString();
        if (!TextUtils.isEmpty(mCurrentFragment) && mCurrentFragment.equals(classname) && compareArgs.equals(mArgs)) return;

        mDrawerLayout.closeDrawers();
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.container, Fragment.instantiate(this, classname, pArgs));
        if (pPopBackStack) {
            ft.addToBackStack(classname);
        }
        ft.commit();

        mCurrentFragment = classname;
    }



    @Override
    protected void onResume() {
        super.onResume();
        createSyncOpperation();
        Events.register(this);

    }

     @Override
    protected void onPause() {
         deleteSyncOperation();
         Events.unregister(this);
         super.onPause();

    }

    @Subscribe
    public void onErrorEvent(ErrorEvent pErrorEvent) {
        if (pErrorEvent != null) {
            String message = pErrorEvent.getMessage();
           if (getViewCrouton() == null) {
               Crouton.makeText(this, String.format("Status: %d, Error: %s, Exeption: %s", pErrorEvent.getStatusCode(), TextUtils.isEmpty(message) ? "not set" : message,  pErrorEvent.getException()), Style.ALERT).show();
           } else {
               Crouton.makeText(this, String.format("Status: %d, Error: %s, Exeption: %s", pErrorEvent.getStatusCode(), TextUtils.isEmpty(message) ? "not set" : message , pErrorEvent.getException()), Style.ALERT, getViewCrouton()).show();
           }
        }
    }


    private void deleteSyncOperation() {
        if (mHandler != null && mRunnuble != null) {
            mHandler.removeCallbacks(mRunnuble);
            mHandler = null;
            mRunnuble = null;
        }
    }

    protected void setCroutonView(ViewGroup pViewCrouton) {
        mViewCrouton = pViewCrouton;
    }


    public ViewGroup getViewCrouton() {
        return mViewCrouton;
    }
}

