package com.youme.app.io.requests;

import android.text.TextUtils;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.youme.app.notify.ErrorEvent;
import com.youme.app.notify.Events;
import com.youme.app.throwable.EErrorMessage;
import com.youme.app.utils.Settings;
import com.youme.app.utils.log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;

/**
 * BaseRequest
 */
public abstract class BaseRequest extends Request<JSONObject> {

    protected static final String PROTOCOL_CHARSET = "utf-8";

    public BaseRequest(int method, String url) {
        super(method, Settings.HOST_SERVER + url, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                int statusCode = (error == null || error.networkResponse == null) ? 0 : error.networkResponse.statusCode;
                EErrorMessage type = EErrorMessage.getByStatusCode(statusCode);
                //Toast.makeText(YMApplication.getContext(), String.format("1 Status: %d, Error: %s", type.getStatusCode(), type.getMessage()), Toast.LENGTH_SHORT).show();
                postError(type, error.getLocalizedMessage());
            }
        });

        setRetryPolicy(new DefaultRetryPolicy(30 * 1000, getRetry(), 1.5f));

    }

    protected static void postError(EErrorMessage pType, Exception pException) {
        if (pException != null && !TextUtils.isEmpty(pException.getLocalizedMessage())) {
            postError(pType, pException.getLocalizedMessage());
        }
    }


    public static void postError(EErrorMessage pType, String pException) {
        Events.post(new ErrorEvent(
                pType.getStatusCode(),
                pType.getMessageString(),
                pType.getColor(), pException));
    }

    protected abstract int getRetry();
    protected JSONObject getJSONBody() {
        return null;
    };

    @Override
    public byte[] getBody() throws AuthFailureError {
        if (getJSONBody() == null) {
            return super.getBody();
        } else {
            try {
               return getJSONBody().toString().getBytes(PROTOCOL_CHARSET);
            } catch (UnsupportedEncodingException uee) {
                postError(EErrorMessage.EL1, uee);
                VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s",
                        getJSONBody().toString(), PROTOCOL_CHARSET);
                return super.getBody();
            }
        }
    }

    @Override
    protected final Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
        String jsonString = null;
        try {

            Log.d("_responce", response == null  ? "null resp" : new String(response.data, PROTOCOL_CHARSET));
            jsonString = new String(response.data,
                    HttpHeaderParser.parseCharset(response.headers));

        return Response.success(new JSONObject(jsonString),
                HttpHeaderParser.parseCacheHeaders(response));
        } catch (Exception e) {
            return Response.error(new VolleyError(e.getMessage()));
        }

    }


    protected abstract void onResponse(JSONObject response) throws JSONException;

    @Override
    protected final void deliverResponse(JSONObject response) {

        if (response == null) {
            log.e("responce is null");
            postError(EErrorMessage.E0, "responce is null");
        } else if (response.has("Error")) {
            try {
                log.e(String.format("responce whit error %d", response.getInt("Error")));
                postError(EErrorMessage.getByStatusCode(response.getInt("Error")), response.getString("Message"));

            } catch (JSONException e) {
                log.e(String.format("responce whit error %s", e.getLocalizedMessage()));
                postError(EErrorMessage.E0, e);

            }
        } else {
            try {
                onResponse(response);
            } catch (JSONException e) {
                postError(EErrorMessage.E0, e);
                log.e(String.format("responce whit error %s", e.getLocalizedMessage()));

            }
        }
    }
}
