package com.youme.app.io.requests;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.RequestQueue;
import com.youme.app.entity.PublicChatRoom;
import com.youme.app.entity.helper.PublicChatRoomHelper;
import com.youme.app.entity.helper.RealmExt;
import com.youme.app.throwable.EErrorMessage;
import com.youme.app.utils.Settings;
import com.youme.app.utils.log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import io.realm.Realm;

/**
 * SyckRoomRequest
 */
public class AddCloudRequest extends BaseRequest {

    public static final String TAG_SYNC_CLOUD = "tag_add_cloud";
    private final String mHash;
    private final String mToken;
    private JSONObject mJSON;



    public static void newInstance(RequestQueue pRequestQueue, String pToken, String pHash) {
        AddCloudRequest request = new AddCloudRequest(pToken, pHash);
        request.setTag(TAG_SYNC_CLOUD + "_" + pHash);
        pRequestQueue.cancelAll(TAG_SYNC_CLOUD + "_" + pHash);
        pRequestQueue.add(request);
    }

    public AddCloudRequest(String pToken, String pHash) {
        super(Method.POST, "add_room");
        mToken = pToken;
        mHash = pHash;

        Realm realm = null;
        try {
            realm = Realm.getDefaultInstance();
            PublicChatRoomHelper helper = new PublicChatRoomHelper(realm);
            PublicChatRoom room = helper.getByHash(mHash);

            if (room != null && room.isValid()) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("room_name", room.getName());
                jsonObject.put("hash", room.getHash());
                jsonObject.put("lat", room.getLat());
                jsonObject.put("lng", room.getLng());
                jsonObject.put("distance", room.getDistance());
                mJSON = jsonObject;
            }
        } catch (JSONException e) {
            postError(EErrorMessage.EL1, e);
            log.e(e);
        } finally {
            if (realm != null) {
                realm.close();
            }
        }

    }

    @Override
    protected JSONObject getJSONBody() {
        return mJSON;
    }


    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        final Map<String, String> headers = new HashMap<>();
        Log.d("_token", mToken);
        headers.put("token", mToken);
        headers.put("Content-Type", "application/json; charset=utf-8");
        return headers;
    }


    @Override
    protected int getRetry() {
        return 0;
    }

    @Override
    protected void onResponse(JSONObject pResponce) throws JSONException {
        final String roomJid = pResponce.getString("jid");
        final String room_name = pResponce.getString("room_name");
        final String hash = pResponce.getString("hash");
        final long creation = pResponce.getLong("creation");
        final double lat = pResponce.getDouble("lat");
        final double lng = pResponce.getDouble("lng");
        final int distance = pResponce.getInt("distance");

        RealmExt.post(new RealmExt.OnRealm() {
            @Override
            public void onRealm(Realm pRealm) {
                PublicChatRoomHelper helper = new PublicChatRoomHelper(pRealm);
                helper.createOrUpdateToRealm(roomJid, room_name, distance, lat, lng, hash, creation, Settings.get().getJID(), true);
            }
        });


    }
}
